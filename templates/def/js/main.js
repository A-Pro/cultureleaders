function scrollToElement(theElement) {
    if (typeof theElement === "string") theElement = document.getElementById(theElement);
    var selectedPosX = 0;
    var selectedPosY = 0;
    while (theElement != null) {
        selectedPosX += theElement.offsetLeft;
        selectedPosY += theElement.offsetTop;
        theElement = theElement.offsetParent;
    }
    window.scrollTo(selectedPosX, selectedPosY);
}

