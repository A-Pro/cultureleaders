!function (d,w) {
    let body = $('body');
    let Style = $('<style></style>').appendTo(body);
    Style.append('.OurChat{position: fixed; right: 20px;bottom: 10px;width: 300px;height: 42px; transform: none; z-index: 9999;-webkit-box-shadow: 0 0 3px rgba(0,0,0,0.1), 0 5px 50px rgba(0,0,0,0.2); box-shadow: 0 0 3px rgba(0,0,0,0.1), 0 5px 50px rgba(0,0,0,0.2); float:left; transition: right 0.5s, height 1.5s;}');
    Style.append('.chat_window{position: absolute; margin: 0 !important;padding: 0 !important;display: block !important;background: transparent !important;height: 100% !important; width: 100% !important; min-width: 100% !important; max-width: 100% !important; min-height: 100% !important; max-height: 100% !important;}');
    Style.append('.OurChat.chat_open{transition: height 1.5s; height: 400px;width: 300px; max-height: calc(100% - 40px); max-width: calc(100% - 40px); }');
    let chat = {};
    let frame = {};
    $('.OurChat_show').on('click',function () {
        if($(this).data('alias')!== undefined){
            add_chat($(this).data('alias'));
        }
        return false;
    });
    function play() {
        let audio = new Audio('/templates/def/chat/msg.mp3');
        audio.autoplay = true;
    }
    function add_chat (alias) {
        if(chat[alias] === undefined) {
            chat[alias] = $('<div id="OurChat_' + alias + '" data-chat_to="' + alias + '" class="OurChat"></div>').appendTo(body);
            chat[alias].css('right', 20 + 'px');
            chat[alias][0].addEventListener("DOMNodeRemoved", function (e) {
                delete(frame[e.target.dataset.chat_to]);
                delete(chat[e.target.dataset.chat_to]);
                update_position();
            }, false);
            frame[alias] = $('<iframe frameborder="0" src="/chat/' + alias + '" class="chat_window"></iframe>').appendTo(chat[alias]);
            update_position();
        }
    }


    function update_position(){
        //alert('обновляем позицию блоков');
        let i=0;
        for (let key in chat) {
            chat[key].css('right',(20 + (310*i))+'px');
            i++;
        };
        // console.log(chat);
    }

    function add_message(fr,name,mess,created){
        let add_class = 'receive';
        if(name === 'Я') {
            add_class = 'sent';
        }
        fr.contents().find('.room-box').append('<div class="row msg_container base_' + add_class + '">'+
            '<div class="col-md-10 col-xs-10">'+
            '<div class="messages msg_' + add_class + '">'+
            '<p>' + mess + '</p>'+
            '<time datetime="' + created + '">' + created + '</time>'+
            '</div></div></div>');
    }

    function new_messages(dt) {
        $.ajax({
            type: 'POST',
            url: '/empty/chat/newmessages',
            data: 'dt='+dt,
            success: function (data) {
                // console.log(data);
                data = JSON.parse(data);
                w.updated = data.new_dt;
                let messages = data.messages;
                // console.log(chat);
                // console.log(typeof messages);
                if(typeof messages === "object"){
                    // console.log(messages);
                    let key;
                    for (key in messages) {
                        if(chat[key]!== undefined){
                            // alert('добавляем сообщения');
                            for(let i=0; i < messages[key].length; i++){

                                add_message(frame[key],messages[key][i].name,messages[key][i].message,messages[key][i].created);
                            }
                            frame[key].contents().find('.room-box').eq(0).scrollTop(frame[key].contents().find('.room-box')[0].scrollHeight);
                        }else{
                            add_chat (key);
                            play();
                        }
                    }
                    // for(let i=0; i < messages.length; i++){
                    //     add_message(messages[i].message,messages[i].created);
                    //     w.updated = messages[i].created;
                    //     $('.room-box').eq(0).scrollTop($('.room-box')[0].scrollHeight);
                    // }
                }
            },
            error: function (xhr, str) {
                console.log('Возникла ошибка: ' + xhr.responseCode);
                clearInterval(timerId);
            }
        });
    }
    let timerId = setInterval(function(){
        new_messages(w.updated);
    }, 2000);

}(document, window );