/**
 * Created by HOME on 11.07.2019.
 */
_LANGUAGE = {};
_LANGUAGE.title = 'заголовок';
_LANGUAGE.enter_title = 'Введите заголовок документа';
_LANGUAGE.enter_new_title = 'Введите новый заголовок документа';
_LANGUAGE.request_del = 'Вы уверены что хотите удалить ветку';
_LANGUAGE.request_del_all = 'Вы уверены что хотите удалить ВСЕ записи ?';
_LANGUAGE.months = [
    'Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'
];
_LANGUAGE.dayOfWeek = [
    "Вск", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"
];

