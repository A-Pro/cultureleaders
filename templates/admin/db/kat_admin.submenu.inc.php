<?
$block = array('search', '', 'add', 'noline');
// OnClick="ShowPopUp('/clear/< ?=$_KAT['MODULE_NAME']? >/< ? =$_KAT['KUR_ALIAS']? >/?import',300,100,'no');return false;"
?>

<div id="db_adm_menu" style="visibility:hidden;position:absolute">
    <ul class="nav nav-pills">

        <?php if ($_KAT[$_KAT['KUR_ALIAS']]['PERM']['ua'] != 1) { ?>
            <li><a href='javascript:;'
                   onclick='ShowPopUp("/clear/<?= $_KAT['MODULE_NAME'] ?>/<?= $_KAT['KUR_ALIAS'] ?>/add?edit&<?= $_SERVER['QUERY_STRING'] ?>", 800, 600, "yes");'><i
                            class="fa fa-file-o"></i>&nbsp;<?= Main::get_lang_str('add', 'db'); ?></a></li>
        <?php } ?>
        <?
        // ������� �������
        global $_KAT, $FORM_IMPORT, $FORM_IMPORTIMG;
        if (!empty($_KAT[$_KAT['KUR_ALIAS']]['IMPORT'])) {
            $FORM_IMPORT = $_KAT[$_KAT['KUR_ALIAS']]['IMPORT'];
        }

        if (!empty($FORM_IMPORTIMG)) {
            ?>
            <!-- �������������� ����������� -->
            <li><A HREF='javascript:;'
                   OnClick='ShowPopUp("/clear/<?= $_KAT['MODULE_NAME'] ?>/<?= $_KAT['KUR_ALIAS'] ?>/mass_add?edit&<?= $_SERVER['QUERY_STRING'] ?>", 800, 600, "yes");'><i
                            class="icon-copy"></i>&nbsp;<?= Main::get_lang_str('multizagr', 'db'); ?></A></li>
        <? } ?>
        <?
        // ���������� �� ���������
        if (!empty($FORM_DATA['prior'])) {
            ?>
            <li><a href='javascript:;'
                   OnClick='ShowPopUp("/clear/<?= $_KAT['MODULE_NAME'] ?>/<?= $_KAT['KUR_ALIAS'] ?>/site_order?<?= $_SERVER['QUERY_STRING'] ?>", 800, 600, "yes");'><i
                            class="fa fa-sort-amount-asc"></i> <?= Main::get_lang_str('priority', 'db'); ?></A></li>
            <?
        }
        ?>
        <?
        // ������� �������
        if (!empty($FORM_IMPORT)) {
            ?>
            <!-- ������������� -->
            <li><a HREF='javascript:;'
                   OnClick="ShowPopUp('/clear/<?= $_KAT['MODULE_NAME'] ?>/<?= $_KAT['KUR_ALIAS'] ?>/action?import',600,450,'yes');return false;"><i
                            class="icon-share"></i>&nbsp;<?= Main::get_lang_str('import', 'db'); ?></a></li>
            <li><a HREF='javascript:;' OnClick='db_empty();'><i
                            class="icon-eraser"></i>&nbsp;<?= Main::get_lang_str('clear', 'db'); ?></a></li>
            <?
        } ?>
        <?php if (!empty($_KAT[$_KAT['KUR_ALIAS']]['EXPORT'])) { ?>
            <?php $FORM_IMPORT = $_KAT[$_KAT['KUR_ALIAS']]['EXPORT']; ?>
            <!-- �������������� -->
            <li>
                <?php $add_req = str_replace('submit=','', $_SERVER['QUERY_STRING']); ?>
                <a href='/clear/<?= $_KAT['MODULE_NAME'] ?>/<?= $_KAT['KUR_ALIAS'] ?>/action?export&<?= $add_req;?>' target="_blank"><i
                            class="icon-share"></i>&nbsp;<?= Main::get_lang_str('export', 'db'); ?></a>
            </li>
        <?php } ?>

        <?php if (!empty($_KAT[$_KAT['KUR_ALIAS']]['admin_search']) && is_array($_KAT[$_KAT['KUR_ALIAS']]['admin_search'])) { ?>
            <?php
            $search_cnt = 0;
            foreach ($_KAT[$_KAT['KUR_ALIAS']]['admin_search'] as $search_field){
                if(!empty($_GET['form'][$search_field])) $search_cnt++;
            }
            ?>
            <!-- ������ ����� �� �������� -->
            <li><a role="button" data-toggle="collapse" href="#<?= $_KAT['KUR_ALIAS']?>_admin_search" aria-expanded="false"
                   aria-controls="<?= $_KAT['KUR_ALIAS']?>_admin_search"><i
                            class="fa fa-filter"></i>&nbsp;<?= Main::get_lang_str('Filter', 'db'); ?>
                    <?= ($search_cnt)? ' '.$search_cnt:'';?>
                </a>
            </li>
            <?php if($search_cnt){ ?>
            <li>
                <a href="/db/<?= $_KAT['KUR_ALIAS']?>/" >
                    <?= Main::get_lang_str('Reset filter', 'db'); ?>
                </a>
            </li>
            <?php } ?>
            <?
        } ?>
    </ul>
</div>
<div id="db_adm_menu_r" style="visibility:hidden;position:absolute">
    <?
    if (!in_array($Cmd, $block)):?>
        <!-- ������������� -->
        <li><a HREF='?edit'><IMG SRC='<?= KAT::inc_link('a_edit.gif', $tmp) ?>' BORDER='0'
                                 ALT='<?= Main::get_lang_str('redakt', 'db'); ?>'></a></li>
        <!-- ������� -->
        <li><a href="#"><IMG SRC='<?= KAT::inc_link('a_delete.gif', $tmp) ?>' BORDER='0'
                             ALT='<?= Main::get_lang_str('del', 'db'); ?>' OnClick='db_del();'
                             align="left">&nbsp;<?= Main::get_lang_str('del', 'db'); ?></a>
            <script language="javascript" type="text/javascript">
                function db_del() {// js
                    if (confirm("<?= Main::get_lang_str('del_request', 'db');?>")) {
                        document.location.href = "http://" + location.host + location.pathname + "?del";
                        return true;
                    } else
                        return false;
                }
            </script>
        </li>
    <? endif; ?>

</div>

<div id="kat_buff_cont" style="visibility:hidden;position:absolute">
    <?
    if (is_array($_SESSION['KAT']['CUTTED_ITEM_ALIAS'])) {
        //echo "<pre>"; print_r($_SESSION['KAT']); echo "</pre>";
        foreach (@$_SESSION['KAT']['CUTTED_ITEM_ALIAS'] as $k => $v) {
            $name_cuted = DOCTXT::get_name_by_alias($v);
            echo "<a href='javascript:;' title=" . $v . ">" . substr($name_cuted, 0, 40) . "(" . $_SESSION['KAT']['CUTTED_TYPE'][$k] . ")</a><br>";
        }
    }
    ?>
</div>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        writeField('CreateCmdLeft', getField('db_adm_menu'));
        writeField('CreateCmdRight', getField('db_adm_menu_r'));
        writeField('LeftPreview', "<p><?=@$message?></p>");
        writeField('Buff', getField('kat_buff_cont'));
    });
</script>
<!--
</div>
-->

