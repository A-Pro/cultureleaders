<?php
$_OBSV['WALL_OFF'] = true;
$_OBSV['PLUGIN'] = 'plugin';
$_OBSV['NO_EMAIL_CHECK'] = true;
$_OBSV['SAVE_DB'] = true;
$_OBSV['TABLE'] = DB_TABLE_PREFIX . 'obsv';
$_OBSV['SEND_FROM_PLUGIN'] = true;


$FORM_DATA = array(
    'id' =>
        array(
            'field_name' => 'id',
            'name' => 'obsv[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array(
            'field_name' => 'alias',
            'name' => 'obsv[alias]',
            'title' => 'alias',
            'must' => 0,
            'maxlen' => 255,
            'type' => 'hidden',
        ),
    'name' =>
        array(
            'field_name' => 'name',
            'name' => 'obsv[name]',
            'title' => 'Дата заказа',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'search' => " LIKE '%%%s%%'",
            'default' => date('Y-m-d H:i:s')
        ),
    'person' =>
        array(
            'field_name' => 'person',
            'name' => 'obsv[person]',
            'title' => 'ФИО',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'email' =>
        array(
            'field_name' => 'email',
            'name' => 'obsv[email]',
            'title' => 'E-mail',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'phone' =>
        array(
            'field_name' => 'phone',
            'name' => 'obsv[phone]',
            'title' => 'Телефон',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'ts' =>
        array(
            'field_name' => 'ts',
            'name' => 'obsv[ts]',
            'title' => 'Дата',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default' => date('Y-m-d')
        ),
    'describe' =>
        array(
            'field_name' => 'describe',
            'name' => 'obsv[describe]',
            'title' => 'Сообщение',
            'must' => 0,
            'maxlen' => '65535',
            'type' => 'textarea',
            'style' => 'width:100%',
            'rows' => '20',
        ),
    'cont' =>
        array(
            'field_name' => 'cont',
            'name' => 'obsv[cont]',
            'title' => 'Письмо',
            'must' => 0,
            'maxlen' => '65535',
            'type' => 'textarea',
            'style' => 'width:100%',
            'rows' => '30',
            'wysiwyg' => 'tinymce',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        )
);
