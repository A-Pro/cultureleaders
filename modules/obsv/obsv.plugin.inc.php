<?php
/* 
incomming: $_POST['obsv']
output: $_POST['obsv'][name], email, body, 
We need to have 
*/
global $obsv, $_CORE, $_CONF,$_OBSV,$SQL_DBLINK;

$oSQL = new SQL();
if(!empty($_POST['obsv']['email']) ){
    if(!empty($_POST['obsv']['agree_eula'])) {

        if (!$_CORE->dll_load('class.lMail'))
            die ($_CORE->error_msg());
        $user_id = (!empty($_SESSION['SESS_AUTH']['ID'])) ? $_SESSION['SESS_AUTH']['ID'] : 0;
        $arr = array(
            'email' => mysqli_real_escape_string($SQL_DBLINK, $_POST['obsv']['email']),
            'surname' => mysqli_real_escape_string($SQL_DBLINK, $_POST['obsv']['surname']),
            'name' => mysqli_real_escape_string($SQL_DBLINK, $_POST['obsv']['name']),
            'agree_eula' => intval($_POST['obsv']['agree_eula']),
            'user_id' => $user_id
            //        'news_subscribe'
        );

        // сохраним данные в ЛК пользователя если они еще не заполнены
        if (!empty($_SESSION['SESS_AUTH']['ID']) ) {
            $upd_set_auth = array();
            if(!STR::is_email($_SESSION['SESS_AUTH']['LOGIN']) && STR::is_email($arr['email'])){
                $upd_set_auth['author_login'] = $arr['email'];
            }
            if(empty($_SESSION['SESS_AUTH']['ALL']['author_surname'])){
                $upd_set_auth['author_surname'] = $arr['surname'];
            }
            if(!empty($upd_set_auth))
                $oSQL->upd(DB_TABLE_PREFIX . 'auth_pers', $oSQL->get_set($upd_set_auth),
                "author_id = '" . $_SESSION['SESS_AUTH']['ID'] . "'");

            if(!STR::is_email($_SESSION['SESS_AUTH']['LOGIN'])) {
                $_SESSION['SESS_AUTH']['LOGIN'] = $arr['email'];
                $_SESSION['SESS_AUTH']['ALL']['author_login'] = $arr['email'];
            }
            if(empty($_SESSION['SESS_AUTH']['ALL']['author_surname'])){
                $_SESSION['SESS_AUTH']['ALL']['author_surname'] = $arr['surname'];
            }

        }

        $lectures = $oSQL->getallkey('*', DB_TABLE_PREFIX . 'events', "event_date >= '" . date('Y-m-d') . "' AND (hidden = 0 OR hidden IS NULL)", 'ORDER BY event_date ASC', 'alias');
        $teachers = $oSQL->getallkey('*', DB_TABLE_PREFIX . 'teachers', "hidden = 0 OR hidden IS NULL", '', 'alias');
        $events = array();
        if (!empty($_POST['lk']) && !empty($user_id)) { // то отчистим записи данного пользователя
            foreach ($lectures as $lec) {
                $oSQL->del(DB_TABLE_PREFIX . 'events_subscribe', "user_id = '" . $user_id . "' AND event_id = '" . $lec['alias'] . "'", DEBUG);
            }
        }
        if (!empty($_POST['obsv']['events'])) {
            foreach ($_POST['obsv']['events'] as $event_id) {

                $arr['event_id'] = mysqli_real_escape_string($SQL_DBLINK, $event_id);
                $arr['alias'] = md5($arr['event_id'] . $arr['email']);
                $set = $oSQL->get_set($arr);
                $oSQL->del(DB_TABLE_PREFIX . 'events_subscribe', "alias = '" . $arr['alias'] . "'", DEBUG);
                $oSQL->ins_set(DB_TABLE_PREFIX . 'events_subscribe', $set, DEBUG);

                $lecture = $lectures[$arr['event_id']];
                $events[$event_id] = $lecture;
                $teachers_name = array();
                foreach (explode(',', $lecture['teacher_id']) as $teacher_id) {
                    $teacher = $teachers[$teacher_id];
                    $teachers_name[] = $teacher['name'];
                }

                $events[$event_id]['teacher']['name'] = implode(', ', $teachers_name);

            }
            if (!empty($events)) {
                ob_start();
                if ($_CORE->comm_inc('mail.subscribe.html', $f, 'obsv')) {
                    include "$f";
                }
                $text = ob_get_contents();
                ob_end_clean();
                $mail = new lMail();

                $data['body'] = $text;
                $data['subject'] = 'Subscribe to the lectures on ' . $_SERVER['HTTP_HOST'];
                $data['subject'] = 'Регистрация на лекции - ' . $_SERVER['HTTP_HOST'];
                $data['mailto'] = $arr['email'];
                $data['name'] = $arr['name'];
                $data['email'] = 'no-reply@' . $_SERVER['HTTP_HOST'];
                $res = $mail->sendhtml($data, array(), false);
            }
        }
    }
    die("Ok");
}
if(isset($_POST['obsv']['contact_mail'])) {
    ob_start();
    if ($_CORE->comm_inc('mail.tmpl.html', $f, 'obsv')) {
        include "$f";
    }
    $text = ob_get_contents();
    ob_end_clean();

    $_POST['obsv']['body'] = $text;
    $obsv['alias'] = $_POST['obsv']['alias'] = time();
    $obsv['ts'] = $_POST['obsv']['ts'] = date('Y-m-d');
    $obsv['name'] = $_POST['obsv']['name'] = date('Y-m-d H:i:s');
    $obsv['person'] = $_POST['obsv']['contact_name'];
    $obsv['email'] = $_POST['obsv']['contact_mail'];
    $obsv['phone'] = $_POST['obsv']['phone'];
    $obsv['cont'] = $_POST['obsv']['cont'] =
        'Имя: ' . $_POST['obsv']['contact_name'] . "\n" .
        'Телефон: ' . $_POST['obsv']['phone'] . "\n" .
        'Вопрос: ' . "\n" . $_POST['obsv']['desc'];

    if (OBSV::check($_POST['obsv'])) {
        $_CORE->dll_load('class.cForm');
        $_OBSV['cForm'] = new cForm($FORM_DATA);
        list($var, $val) = $_OBSV['cForm']->GetSql('insert');
        $sql = new SQL();
        $res = $sql->ins($_OBSV['TABLE'], $var, $val, DEBUG);

        if (!$_CORE->dll_load('class.lMail'))
            die ($_CORE->error_msg());

        $mail = new lMail();

        $data['body'] = $text;
        $data['subject'] = 'Request from ' . $_SERVER['HTTP_HOST'];
        $data['mailto'] = TO;
        $data['name'] = TO;
        $data['email'] = 'no-reply@' . $_SERVER['HTTP_HOST'];
        //$data['smtp'] = $_CONF['FEATURES_USED']['smtp'];

        $res = $mail->sendhtml($data, array(), true);

    } else {
        $_OBSV['ERROR'] = '';
    }
}