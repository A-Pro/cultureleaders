<?php
#
# LANG RU
#

define ("OBSV_ERR_WRONG_EMAIL","Неверный e-mail адрес.");
define ("OBSV_ERR_EMPTY_BODY","Пустое письмо.");
define ("OBSV_ERR_LARGE_BODY","Слишком длинное письмо.");
define ("OBSV_ERR_EMPTY_NAME","Введите Ваше имя.");

define ("OBSV_ERR_NOT_SEND",'Ваш вопрос не удалось отправить, попробуйте позднее.');
define ("OBSV_ERR_SEND",'Ваш вопрос отправлен.');

