<?php
global $_CORE, $_CONF, $_ACCESS, $_DASHBOARD;

if (empty($_SESSION['SESS_AUTH']['ID'])) {
    
    header('Location: /oauth/login');
    exit;
}
if (empty($_SESSION['SESS_AUTH']['ALL']['alias']) || empty($_SESSION['SESS_AUTH']['LOGIN'])) {
    header('Location: /auth/logoff');
    exit;
}
include_once "cabinet.inc.php";

$LCab = new Cabinet();

switch ($Cmd):
    case 'content/title/seo_title':
    case 'content/title/short':
    case 'content/title':
        echo $LCab->get_title();
        break;
    case 'content/seo_keywords':
    case 'content/seo_desc':
        echo '';
        break;
    case '' :
        // обновляем сессионные переменные пользователя
        $LCab->auth_update();

        /*
         * тут получаем необходимые данные
         */
        $nomination = false;
        $mentor = false;
        $project_status = false;
        if(!empty($_SESSION['SESS_AUTH']['ALL']['code_project']) &&
            $_SESSION['SESS_AUTH']['ALL']['role_project']>= '20'){
            $nomination_id = $LCab->get_nom_id($_SESSION['SESS_AUTH']['ALL']['code_project']);
            $project_status = $LCab->get_project_status($_SESSION['SESS_AUTH']['ALL']['code_project']);
            if(!empty($nomination_id)) {
                $_SESSION['SESS_AUTH']['ALL']['project_nom'] = $nomination_id;
                $nomination = $LCab->get_nomination($nomination_id);
                $mentor = $LCab->get_mentor($nomination_id);
            }
        }
        $_SESSION['status_project'] = $project_status;

        // подключаем шаблон
        $LCab->show_cabinet(compact('nomination','mentor','project_status'));
        
        break;
    case 'projects':

        if($_SESSION['SESS_AUTH']['ALL']['account_type'] == ACCTYPE_ROOT) {
            if(empty($_SESSION['SESS_AUTH']['ALL']['code_project'])) {
                $_GET['edit'] = '1';
                if ($_CORE->comm_inc('add_project.inc.html', $file, 'cabinet'))
                    include $file;
            }else{
                if($_SESSION['SESS_AUTH']['ALL']['role_project']>= '20'){
                    if ($_CORE->comm_inc('view_project.inc.html', $file, 'cabinet'))
                        include $file;
                }else{
                    if ($_CORE->comm_inc('add_project.inc.html', $file, 'cabinet'))
                        include $file;
                }
            }
        }

        break;

    default:
        ob_start();
        echo cmd($Cmd,true,false);
        $content = ob_get_contents();
        ob_end_clean();
        // подключаем шаблон
        $LCab->show_cabinet(array('content'=>$content));
    
endswitch;
