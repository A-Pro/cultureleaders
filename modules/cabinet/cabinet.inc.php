<?php
Main::load_lang_mod('auth');
class Cabinet
{
    private $sql;

    public function __construct()
    {
        $this->sql = new SQL();
    }

    public function get_title()
    {
        global $_DASHBOARD;

        $title = 'Личный кабинет';
        if($_SESSION['SESS_AUTH']['ALL']['account_type'] == ACCTYPE_ROOT)
            $title .= " участника";
        if($_SESSION['SESS_AUTH']['ALL']['account_type'] == ACCTYPE_MENTOR)
            $title .= " ментора";
        $_DASHBOARD['title'] = $title;
        return $title;
    }


    public function show_cabinet($vars=array())
    {
        global $_CORE, $_CONF;
        extract($vars);
        // if($_SESSION['SESS_AUTH']['ALL']['account_type'] == ACCTYPE_ROOT)
            $page = 'default.tpl.html';
        if($_SESSION['SESS_AUTH']['ALL']['account_type'] == ACCTYPE_MENTOR)
            $page = 'mentor.tpl.html';
        if ($_CORE->comm_inc($page, $file, 'cabinet'))
            include $file;
    }


    public function auth_update()
    {
        $fields = array('account_type','code_project','role_project','nomination_id');
        $auth_data = $this->sql->getrow(implode(',',$fields), DB_TABLE_PREFIX.'auth_pers','author_id = '.$_SESSION['SESS_AUTH']['ID']);
        foreach ($fields as $field){
            $_SESSION['SESS_AUTH']['ALL'][$field] = $auth_data[$field];
        }
    }

    public function get_nom_id($code_project)
    {
        $nomination_id = $this->sql->getval(
            'nomination_id',
            DB_TABLE_PREFIX.'projects',
            "alias = '".$code_project."'");
        return $nomination_id;
    }

    public function get_project_status($code_project)
    {
        $status = $this->sql->getval(
            'status',
            DB_TABLE_PREFIX.'projects',
            "alias = '".$code_project."'");
        return $status;
    }

    public function get_nomination($nom_id)
    {
        $nomination = $this->sql->getrow(
            '*',
            DB_TABLE_PREFIX.'nominations',
            "alias = '".$nom_id."'");
        return $nomination;
    }

    public function get_mentor($nom_id)
    {
        $mentor = $this->sql->getrow(
            'author_comment',
            DB_TABLE_PREFIX.'auth_pers',
            "nomination_id = '".$nom_id."'");
        return $mentor;
    }
}