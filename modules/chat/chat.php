<?php
include_once "chat.inc.php"; 

global $_PROJECT, $_CHAT, $_CORE, $SQL_DBCONF, $_KAT; // in user/index.inc

if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

list($tab, $command)        = $_CORE->cmd_parse($Cmd);

switch ($Cmd) {
	case "content/title":
	case "module/title":
		echo "Чат";
		break;
	case "content/path":
	case "module/desc":
		break;


    case "newmessages":
        $updated = $_POST['dt'];

        if(empty($updated))
            die(json_encode(array("error"=> "empty datetime")));

        $oChat      = new CHAT;
        $data = $oChat->get_new_messages($_SESSION['SESS_AUTH']['ALL']['alias'], $updated);

        $history = array();
        $contacts = array();
        if(empty($data))  $data = array();
        foreach ($data as $item){

            if( empty($contacts[$item['room']])){
                $contacts[$item['room']] = $oChat->get_users_to_room($item['room']);
            }
            $to_user = '';
            foreach ($contacts[$item['room']] as $alias=>$contact){
                if($alias!=$_SESSION['SESS_AUTH']['ALL']['alias']){
                    $to_user = $alias;
                }else{
                    $contacts[$item['room']][$alias] = 'Я';
                }
            }
            $history[$to_user][] = array(
                'name' => $contacts[$item['room']][$item['from']],
                'message' => $item['message'],
                'created' => $item['created'],
            );
        }
        die(json_encode(array('messages'=> $history, 'new_dt' => date('Y-m-d H:i:s'))));
        break;

    case "history":
        $to_user = $_POST['to'];
        $updated = $_POST['dt'];
        if(empty($to_user))
            die(json_encode(array("error"=> "empty to_user")));

        if(empty($updated))
            die(json_encode(array("error"=> "empty datetime")));

        if($to_user == $_SESSION['SESS_AUTH']['ALL']['alias'])
            die(json_encode(array("error"=> "message to yourself"))); // нельзя переписываться с самим собой

        $oChat      = new CHAT;
        if(!$oChat->check_user($to_user))
            die(json_encode(array("error"=> "the user is not found"))); // нет пользователя с таким alias? - закончим выполнение
        $room = $oChat->generate_room($to_user,$_SESSION['SESS_AUTH']['ALL']['alias']);
//        $message = STR::encode_emoji($message);
        $users_to_room = $oChat->get_users_room($to_user);
        $data = $oChat->get_history($room, array('updated' => $updated));
        $history = array();
        if(empty($data))  $data = array();
        foreach ($data as $item){
            $history[] = array(
                'name'=> $users_to_room[$item['from']]['author_comment'],
                'message' => $item['message'],
                'created' => $item['created'],
            );
        }
        die(json_encode(array('messages'=> $history)));
        break;

    case "send":
        $to_user = $_POST['to'];
        $message = $_POST['message'];
        if(empty($to_user))
            die(json_encode(array("error"=> "empty 'to' data")));

        if(empty($message))
            die(json_encode(array("error"=> "empty message")));

        if($to_user == $_SESSION['SESS_AUTH']['ALL']['alias'])
            die(json_encode(array("error"=> "message to yourself"))); // нельзя переписываться с самим собой

        $oChat      = new CHAT;
        if(!$oChat->check_user($to_user))
            die(json_encode(array("error"=> "the user is not found"))); // нет пользователя с таким alias? - закончим выполнение
        $room = $oChat->generate_room($to_user,$_SESSION['SESS_AUTH']['ALL']['alias']);
//        $message = STR::encode_emoji($message);
        if($oChat->save_message($room, $_SESSION['SESS_AUTH']['ALL']['alias'], $message)){
            die("Ok");
        }else{
            die(json_encode(array("error"=> "message was not saved")));
        }
        break;


    case 'test':

        if(Main::comm_inc('user_chat.html',$f, 'chat'))
            include $f;
        die;
        break;


    default:
        if( empty($_SESSION['SESS_AUTH']['ALL']['alias']) ) break; // неавторизован - на выход

        if(empty($Cmd)) break; // пустая команда? - закончим выполнение
        if($Cmd == $_SESSION['SESS_AUTH']['ALL']['alias']) break; // нельзя переписываться с самим собой
        $oChat      = new CHAT;
        if(!$oChat->check_user($Cmd)) break; // нет пользователя с таким alias? - закончим выполнение
        $to_user = $Cmd;
        $room = $oChat->get_room($to_user,$_SESSION['SESS_AUTH']['ALL']['alias']);
        if(empty($room)) die('ошибка');
        $users_to_room  = $oChat->get_users_room($to_user);
        $history        = $oChat->get_history($room);

        if(Main::comm_inc('chat.inc.html',$f, 'chat'))
            include $f;
        die;
        break;
}