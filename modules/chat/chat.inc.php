<?php global $_SERVER;

class CHAT
{
    public $SERVER;
    public $SQL_DBCONF;

    function __construct()
    {
        $this->sql = new SQL();
    }

    function get_online($recip, $notuser)
    {
        $users = $this->sql->getall('alias, author_comment, author_login, online', DB_TABLE_PREFIX . 'auth_pers', "from_auth = {$recip} and author_id != {$notuser}","ORDER BY author_comment");
        if (!empty($users)){
            foreach ($users as $i => $user){
                $users[$i]['room'] = $this->generateRoom($_SESSION['SESS_AUTH']['ALL']['alias'],$user['alias']);
            }
            return $users;
        }else
            return array();
    }

    public function get_contacts($user_id)
    {
        $user = $this->sql->getrow('author_id, alias, author_comment, author_login, online, psych, psych_id', DB_TABLE_PREFIX . 'auth_pers', "author_id = {$user_id}","LIMIT 1");

        $users = array();
        if($user['psych']) {
            $users = $this->sql->getall('alias, author_comment, author_login, online', DB_TABLE_PREFIX . 'auth_pers', " psych_id = {$user_id}", "ORDER BY author_comment");
        }else{
            $users[] = $this->sql->getrow('alias, author_comment, author_login, online', DB_TABLE_PREFIX . 'auth_pers', "author_id = {$user['psych_id']}","LIMIT 1");
        }

        if (!empty($users)) {
            foreach ($users as $i => $user2) {
                $users[$i]['room'] = $this->generateRoom($user['alias'], $user2['alias']);
            }
        }

        return $users;
    }


    public function get_history($room, $filter=array())
    {
        $where = "room ='{$room}'";
        $tail = "ORDER BY `created` ASC";

        /*$limit = (!empty($filter['limit'])) ? $filter['limit']: '30';
        $tail .= " LIMIT ".$limit;*/

        if(!empty($filter['timestamp']))
            $where .= " AND created < '{$filter['timestamp']}'";

        if(!empty($filter['updated']))
            $where .= " AND created > '{$filter['updated']}'";

        $messages = $this->sql->getall('*', DB_TABLE_PREFIX . 'chat_rooms_history', $where, $tail);

        if (!empty($messages)) {
            return $messages;
        }else
            return array();
    }




    public function get_new_messages($user, $updated)
    {
        $messages = $this->sql->getall('t1.*',
            DB_TABLE_PREFIX . 'chat_rooms_history as t1 '.
                'LEFT JOIN '.DB_TABLE_PREFIX . "chat_rooms as t2 ON t1.room = t2.room AND t2.user ='{$user}'",
            "t1.created > '{$updated}'",
            'ORDER BY t1.id ASC',
            0
        );

        if (!empty($messages)) {
            return $messages;
        }else
            return array();
    }

    /**
     * Возвращает общее кол-во входящих сообщений от определенного контакта
     * @param $room идентификатор комнаты
     * @param $from алиас от кого сообщение
     * @return integer кол-во сообщений
     */
    public function inbox_cnt($room, $from)
    {
        $where = "`room` ='{$room}' AND `from` = '{$from}'";
        $count_messages = $this->sql->getval('count(*)', DB_TABLE_PREFIX . 'chat_rooms_history', $where);
        return $count_messages;
    }

    function set_room($u1, $u2, $room)
    {
        $set = array('user' => $u1, 'room' => $room);
        $res1 = $this->sql->ins_set(DB_TABLE_PREFIX . 'chat_rooms', $set);

        $set = array('user' => $u2, 'room' => $room);
        $res2 = $this->sql->ins_set(DB_TABLE_PREFIX . 'chat_rooms', $set);

        if ( !$res1->Result || !$res2->Result ) {
            echo("\nError : " . $res1->ErrorQuery . "\n");
            echo("\nError : " . $res2->ErrorQuery . "\n");
            $this->sql->del(DB_TABLE_PREFIX . 'chat_rooms',"room = '{$room}'");
            //удалим если есть ошибки, чтобы не засорять
            return false;
        }

        return true;

    }

    public function get_room($u1, $u2)
    {

        $room = $this->generate_room($u1, $u2);

        $result = $this->sql->getval('count(*)', DB_TABLE_PREFIX . 'chat_rooms', "room = '{$room}'", "LIMIT 1");
        if(empty($result) || $result < 2){
            if(!$this->set_room($u1, $u2, $room)){
                return false;
            }
        }
        return $room;

    }

    public function get_rooms_user($u)
    {
        $rooms = $this->sql->getvals('room', DB_TABLE_PREFIX . 'chat_rooms', "user = '{$u}'");
        if(empty($rooms)){
            $rooms = array();
        }
        return $rooms;
    }

    public function get_users_room($alias)
    {

        $users = array();
        $users[$_SESSION['SESS_AUTH']['ALL']['alias']] = array(
            'author_comment' => 'Я'
        );
        if(!empty($alias)){
            $user = $this->sql->getrow('*', DB_TABLE_PREFIX . 'auth_pers', "alias = '{$alias}'","",DEBUG);
            if(!empty($user)){
                $users[$user['alias']] = array(
                    'author_comment' => $user['author_comment']
                );
            }
        }

        return $users;

    }

    public function get_users_to_room($room)
    {

        $users = array();
        if(!empty($room)){
            $res = $this->sql->getall('t1.*',
                DB_TABLE_PREFIX . 'auth_pers as t1 '.
                    'LEFT JOIN '.DB_TABLE_PREFIX . 'chat_rooms as t2 ON t2.`user`=t1.alias',
                "t2.room = '{$room}'",
                "",
                DEBUG
            );
            if(!empty($res)){
                foreach ($res as $item){
                    $users[$item['alias']] = array(
                        'author_comment' => $item['author_comment']
                    );
                }
            }
        }

        return $users;

    }

    function save_message($room, $from, $message)
    {

        $data = array(
            'room' => $room,
            'message' => $message,
            'from' => $from
        );

        $result = $this->sql->ins_set(DB_TABLE_PREFIX . 'chat_rooms_history', $data, DEBUG);
        if (!$result->Result) {
            echo("\nError : " . $result->ErrorQuery . "\n");
            return false;
        }
        return true;

    }

    public function check_user($alias)
    {
        $result = $this->sql->getval('count(*)', DB_TABLE_PREFIX . 'auth_pers', "`alias` = '{$alias}'");
        return $result;
    }

    public function generate_room($alias1, $alias2)
    {
        $arr = array($alias1, $alias2);
        sort($arr);
        reset($arr);
        return md5(implode('', $arr));
    }

}