<?php global $_CORE;
//$_KAT['coredb_files']['hidden']			= ''; // для db/list
//$_KAT['coredb_images']['hidden']			= 'true'; // для db/list
//$_KAT['coredb_youtube']['hidden']			= 'true'; // для db/list

$_KAT['rss']['news'] = false;

$_KAT['conf']['SEO'] = false;
$_KAT['conf']['SEO_PAGES'] = false;

unset($_KAT['ADM_MENU']["/db/auth/"]);

define ("KAT_ERR_NOT_FOUND","Ничего не найдено");


$_KAT['coredb_users']['hidden']	= true;


$_KAT['TABLES']['settings']	= 'settings';
$_KAT['SRC']['settings']		= 'settings';
$_KAT['TITLES']['settings']	= Main::get_lang_str('settings', 'db');
$_KAT['FILES']['settings']	    = 'settings.data.inc.php';
$_KAT['settings']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['settings']	= '/tmp/inside';
$_KAT['settings']['AFTER_ADD_CMD']	= '/css/restyle';
$_KAT['settings']['NOSEO'] = true;
$_KAT['INDEX']['settings']     = 'alias';

$_KAT['TABLES']['manuals']	= 'manuals';
$_KAT['SRC']['manuals']		= 'manuals';
$_KAT['TITLES']['manuals']	= 'Методические материалы';
$_KAT['FILES']['manuals']	    = 'manuals.data.inc.php';
$_KAT['manuals']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['manuals']	= '/tmp/inside';
$_KAT['manuals']['NOSEO']     = true;
$_KAT['INDEX']['manuals']      = 'alias';
$_KAT['manuals']['acc_history'] = true;



$_KAT['auth_pers']['acc_history'] = true;

$_KAT['TABLES']['accounts']	= 'auth_pers';
$_KAT['SRC']['accounts']		= 'accounts';
$_KAT['TITLES']['accounts']	= Main::get_lang_str('accounts', 'db');
$_KAT['FILES']['accounts']	    = 'accounts.data.inc.php';
$_KAT['accounts']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['accounts']	= '/tmp/inside';
$_KAT['accounts']['NOSEO'] = true;
$_KAT['INDEX']['accounts']      = 'author_id';
$_KAT['accounts']['adm_fields'] = array('author_login','author_comment','account_type','ts','hidden');
$_KAT['accounts']['acc_history'] = true;

$_KAT['TABLES']['news']         = 'news';
$_KAT['SRC']['news']            = 'news';
$_KAT['TITLES']['news']         = Main::get_lang_str('news', 'db');
$_KAT['FILES']['news']          = 'news.data.inc.php';
$_KAT['news']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['news']       = '/tmp/inside';
$_KAT['news']['NOSEO']          = true;
$_KAT['onpage_def']['news']     = 20;
$_KAT['news']['acc_history']    = true;
$_KAT['INDEX']['news']          = 'alias';

$_KAT['TABLES']['notes']         = 'notes';
$_KAT['SRC']['notes']            = 'notes';
$_KAT['TITLES']['notes']         = 'Записки';
$_KAT['FILES']['notes']          = 'notes.data.inc.php';
$_KAT['notes']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['notes']       = '/tmp/inside';
$_KAT['notes']['NOSEO']          = true;
$_KAT['onpage_def']['notes']     = 99;
$_KAT['notes']['acc_history']    = true;
$_KAT['INDEX']['notes']          = 'alias';

$_KAT['TABLES']['faq']	= 'faq';
$_KAT['SRC']['faq']		= 'faq';
$_KAT['TITLES']['faq']	= 'Вопросы';
$_KAT['FILES']['faq']	    = 'faq.data.inc.php';
$_KAT['faq']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['faq']	= '/tmp/inside';
$_KAT['faq']['NOSEO']   = true;
$_KAT['onpage_def']['faq']	= 20;
$_KAT['faq']['acc_history'] = true;
$_KAT['INDEX']['faq']     = 'id';

$_KAT['TABLES']['open_lectures']	= 'translations';
$_KAT['SRC']['open_lectures']		= 'open_lectures';
$_KAT['TITLES']['open_lectures']	= 'Записи открытых лекций';
$_KAT['FILES']['open_lectures']	    = 'open_lectures.data.inc.php';
$_KAT['open_lectures']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['open_lectures']	= '/tmp/inside';
$_KAT['open_lectures']['NOSEO']     = true;
$_KAT['onpage_def']['open_lectures']	= 20;
$_KAT['open_lectures']['acc_history'] = true;
$_KAT['INDEX']['open_lectures']     = 'alias';

$_KAT['TABLES']['partners']         = 'partners';
$_KAT['SRC']['partners']            = 'partners';
$_KAT['TITLES']['partners']         = 'Информационные партнеры';
$_KAT['FILES']['partners']          = 'partners.data.inc.php';
$_KAT['partners']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['partners']       = '/tmp/inside';
$_KAT['partners']['NOSEO']          = true;
$_KAT['onpage_def']['partners']     = 99;
$_KAT['INDEX']['partners']          = 'alias';

// секция для настроек другого отображения каталогов (например на главной)
if ($_CORE->CURR_TPL != 'admin') {
    $_KAT['TABLES']['news_small']	= 'news';
    $_KAT['SRC']['news_small']		= 'news_small';
    $_KAT['TITLES']['news_small']	= Main::get_lang_str('news', 'db');
    $_KAT['FILES']['news_small']	    = 'news.data.inc.php';
    $_KAT['news_small']['hidden']	= 'true'; // для db/list
    $_KAT['DOC_PATH']['news_small']	= '/tmp/inside';
    $_KAT['news_small']['NOSEO'] = true;
    $_KAT['onpage_def']['news_small']	= 3;
    $_KAT['news_small']['NO_PLINE']         = true;
    $_KAT['news_small']['page_one']         = true;

    $_KAT['TABLES']['experts_small']	= 'experts';
    $_KAT['SRC']['experts_small']		= 'experts_small';
    $_KAT['TITLES']['experts_small']	= 'Эксперты';
    $_KAT['FILES']['experts_small']	    = 'experts.data.inc.php';
    $_KAT['experts_small']['hidden']	= 'true'; // для db/list
    $_KAT['DOC_PATH']['experts_small']	= '/tmp/inside';
    $_KAT['experts_small']['NOSEO'] = true;
    $_KAT['onpage_def']['experts_small']	= 99;
    $_KAT['experts_small']['NO_PLINE']         = true;
    $_KAT['experts_small']['page_one']         = true;

    $_KAT['TABLES']['mentors_small'] = 'mentors';
    $_KAT['SRC']['mentors_small'] = 'mentors_small';
    $_KAT['TITLES']['mentors_small'] = 'Менторы';
    $_KAT['FILES']['mentors_small'] = 'mentors.data.inc.php';
    $_KAT['mentors_small']['hidden'] = 'true'; // для db/list
    $_KAT['DOC_PATH']['mentors_small'] = '/tmp/inside';
    $_KAT['mentors_small']['NOSEO'] = true;
    $_KAT['onpage_def']['mentors_small'] = 99;
    $_KAT['mentors_small']['NO_PLINE'] = true;
    $_KAT['mentors_small']['page_one'] = true;

    $_KAT['TABLES']['teachers_small'] = 'teachers';
    $_KAT['SRC']['teachers_small'] = 'teachers_small';
    $_KAT['TITLES']['teachers_small'] = 'Преподаватели';
    $_KAT['FILES']['teachers_small'] = 'teachers.data.inc.php';
    $_KAT['teachers_small']['hidden'] = 'true'; // для db/list
    $_KAT['DOC_PATH']['teachers_small'] = '/tmp/inside';
    $_KAT['teachers_small']['NOSEO'] = true;
    $_KAT['onpage_def']['teachers_small'] = 99;
    $_KAT['teachers_small']['NO_PLINE'] = true;
    $_KAT['teachers_small']['page_one'] = true;

    $_KAT['TABLES']['open_lectures_small']	= 'translations';
    $_KAT['SRC']['open_lectures_small']		= 'open_lectures_small';
    $_KAT['TITLES']['open_lectures_small']	= 'Записи открытых лекций';
    $_KAT['FILES']['open_lectures_small']	= 'open_lectures.data.inc.php';
    $_KAT['open_lectures_small']['hidden']	= 'true'; // для db/list
    $_KAT['DOC_PATH']['open_lectures_small']	= '/tmp/inside';
    $_KAT['open_lectures_small']['NOSEO']     = true;
    $_KAT['INDEX']['open_lectures_small']     = 'alias';
    $_KAT['onpage_def']['open_lectures_small'] = 2;
    $_KAT['open_lectures_small']['NO_PLINE'] = true;
    $_KAT['open_lectures_small']['page_one'] = true;
}


/******** справочники **********/
$_KAT['regions']['NOSEO'] = true;
$_KAT['onpage_def']['regions']	= 20;
$_KAT['TABLES']['regions']		= 'regions';
$_KAT['TITLES']['regions']		= 'Регионы';
$_KAT['regions']['AFTER_ADD']	= 'db/regions/';
$_KAT['FILES']['regions']		= 'regions.data.inc.php';
$_KAT['regions']['hidden']		= 'true'; // для db/list
$_KAT['DOC_PATH']['regions']		= '/tmp/inside';
$_KAT['regions']['acc_history'] = true;


$_KAT['nominations']['NOSEO'] = true;
$_KAT['onpage_def']['nominations']	= 20;
$_KAT['TABLES']['nominations']		= 'nominations';
$_KAT['TITLES']['nominations']		= 'Номинации';
$_KAT['nominations']['AFTER_ADD']	= 'db/nominations/';
$_KAT['FILES']['nominations']		= 'nominations.data.inc.php';
$_KAT['nominations']['hidden']		= 'true'; // для db/list
$_KAT['DOC_PATH']['nominations']		= '/tmp/inside';
$_KAT['nominations']['acc_history'] = true;

/******** конец справочников **************/


$_KAT['projects']['NOSEO'] = true;
$_KAT['onpage_def']['projects']	= 20;
$_KAT['TABLES']['projects']		= 'projects';
$_KAT['TITLES']['projects']		= 'Проекты';
$_KAT['projects']['AFTER_ADD']	= 'db/projects/';
$_KAT['FILES']['projects']		= 'projects.data.inc.php';
$_KAT['projects']['hidden']		= 'true'; // для db/list
$_KAT['DOC_PATH']['projects']		= '/tmp/inside';
$_KAT['projects']['acc_history'] = true;


if(isset($_SESSION['SESS_AUTH']['ALL']['account_type'])){
    // подключим конфиг каталогов соответствующий типу аккаунта
    switch ($_SESSION['SESS_AUTH']['ALL']['account_type']) {
        case ACCTYPE_ROOT:
            include 'config/root.conf.inc.php';
            break;
        case ACCTYPE_MENTOR:
            include 'config/mentor.conf.inc.php';
            break;
        case ACCTYPE_EDIT:
            include 'config/edit.conf.inc.php';
            break;
        case ACCTYPE_ORG:
            include 'config/org.conf.inc.php';
            break;
        case ACCTYPE_MODER:
            include 'config/moder.conf.inc.php';
            break;
        case ACCTYPE_ADM:
            include 'config/adm.conf.inc.php';
            break;
    }
}
/*
    VIEW

  CREATE ALGORITHM = UNDEFINED VIEW `lki__accounts` AS SELECT
    t1.*,
    CONCAT(t1.author_comment,' ',t1.author_surname,' ',t1.author_patronymic) as full_name,
    t2.`status` AS status_project,
    t2.`name` AS name_project,
    t3.`name` AS region
FROM
    `lki__auth_pers` AS t1
LEFT JOIN `lki__projects` AS t2
ON
    t2.`alias` = t1.`code_project`
LEFT JOIN `lki__regions` AS t3
ON
    t3.`id` = t1.`region_id`
WHERE
    t1.role_project >= 20;

CREATE ALGORITHM = UNDEFINED VIEW `lki__projects_view` AS
SELECT t1.alias, t1.name, t1.cont, IF(t1.doc != '', CONCAT('https://cultureleaders.ru/data/db/f_projects/',t1.doc),'') as present_1,
count(t2.alias) as cnt_user, t3.name as region, t4.name as nomination, GROUP_CONCAT('https://cultureleaders.ru/data/db/f_docs/',t5.doc) as present_2

FROM `lki__projects` as t1
LEFT JOIN `lki__auth_pers` as t2 ON t2.code_project = t1.alias AND t2.role_project >=20
LEFT JOIN `lki__regions` as t3 ON t3.id = t2.region_id
LEFT JOIN `lki__nominations` as t4 ON t4.alias = t1.nomination_id
LEFT JOIN `lki__docs` as t5 ON t5.project_id = t1.alias AND t5.ts > '2019-10-14' AND t5.ts <= '2019-10-27'
WHERE t1.`status` = 40
GROUP BY t1.alias
ORDER BY `cnt_user` ASC;


*/

