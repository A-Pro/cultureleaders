<?php

$FORM_WHERE = '';

if (!$_CORE->IS_ADMIN) {
    $FORM_ORDER = ' ';
} else {
    $FORM_ORDER = '  ';
}
if (!$_CORE->IS_ADMIN) {
    $FORM_WHERE = "AND (hidden != 1 OR hidden IS NULL) ";
} else {
    $FORM_WHERE = "";
}


$FORM_DATA = array(
    /*	'delimiter1' => array(
            'type' 		=> 'delimeter_line',
            'subtype' 	=> 'tabs',
            'title' 	=> 'Общие настройки'
        ),
    */
    'id' =>
        array(
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array(
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 1,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'ts' =>
        array(
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default' => date('Y-m-d')
        ),
    'name' =>
        array(
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Название пакета настроек',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'sitename' =>
        array(
            'field_name' => 'sitename',
            'name' => 'form[sitename]',
            'title' => 'Название проекта',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'phone' =>
        array(
            'field_name' => 'phone',
            'name' => 'form[phone]',
            'title' => 'Контактный телефон',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'email' =>
        array(
            'field_name' => 'email',
            'name' => 'form[email]',
            'title' => 'Контактный E-mail',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'vk' =>
        array(
            'field_name' => 'vk',
            'name' => 'form[vk]',
            'title' => 'VKontakte',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'fb' =>
        array(
            'field_name' => 'fb',
            'name' => 'form[fb]',
            'title' => 'Facebook',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'twitter' =>
        array(
            'field_name' => 'twitter',
            'name' => 'form[twitter]',
            'title' => 'Twitter',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'instagram' =>
        array(
            'field_name' => 'instagram',
            'name' => 'form[instagram]',
            'title' => 'Instagram',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'hidden' =>
        array(
            'field_name' => 'hidden',
            'name' => 'form[hidden]',
            'title' => Main::get_lang_str('ne_publ', 'db'),
            'must' => 0,
            'maxlen' => 1,
            'type' => 'checkbox',
        ),
    'stage1_off' =>
        array(
            'field_name' => 'stage1_off',
            'name' => 'form[stage1_off]',
            'title' => 'Дата завершения 1-го этапа',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage2_on' =>
        array(
            'field_name' => 'stage2_on',
            'name' => 'form[stage2_on]',
            'title' => 'Завершение отбора достойных проектов',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage2_off' =>
        array(
            'field_name' => 'stage2_off',
            'name' => 'form[stage2_off]',
            'title' => 'Дата завершения 2-го этапа',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage3_on' =>
        array(
            'field_name' => 'stage3_on',
            'name' => 'form[stage3_on]',
            'title' => 'Завершение выбора номинантов',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage3_off' =>
        array(
            'field_name' => 'stage3_off',
            'name' => 'form[stage3_off]',
            'title' => 'Дата завершения 3-го этапа',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),

    'final' =>
        array(
            'field_name' => 'final',
            'name' => 'form[final]',
            'title' => 'Подведение итогов и определение победителей',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
);
