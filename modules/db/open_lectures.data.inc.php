<?php
global $_CORE,  $_CONF;
$FORM_ORDER	= ' ORDER BY ts DESC ';
if (!$_CORE->IS_ADMIN)
    $FORM_WHERE	= " AND online = 0 AND ts != '".date('Y-m-d')."' AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => Main::get_lang_str('name', 'db'),
            'must' => 1,
            'style'	=> 'width:100%',
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'code'	=>
        array (
            'field_name' => 'code',
            'name' => 'form[code]',
            'title' => 'Ссылка на YouTube ролик',
            'must' => 1,
            'style'	=> 'width:100%',
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'ts'	=>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => 'Дата трансляции',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'default'	=> date('Y-m-d')
        ),
    'hidden'	=>
        array (
            'field_name' => 'hidden',
            'name' => 'form[hidden]',
            'title' => Main::get_lang_str('ne_publ', 'db'),
            'must' => 0,
            'maxlen' => 1,
            'type' => 'checkbox'
        ),
);