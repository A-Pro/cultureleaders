<?php global  $_CORE, $FORM_WHERE, $FORM_ORDER,$FORM_FIELD4ALIAS;
$FORM_ORDER	= ' ORDER BY fix DESC, date_on DESC, ts DESC ';
$FORM_WHERE = '';
if (!$_CORE->IS_ADMIN )
    $FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL) AND date_on <= '".date('Y-m-d')."'";
$FORM_FIELD4ALIAS = 'alias';
$FORM_DATA= array (
  'id' => 
  array (
    'field_name' => 'id',
    'name' => 'form[id]',
    'title' => 'id',
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
  ),
  'alias' => 
  array (
    'field_name' => 'alias',
    'name' => 'form[alias]',
    'title' => Main::get_lang_str('alias', 'db'),
    'must' => 0,
    'maxlen' => 20,
    'type' => 'hidden',
      'default' => uniqid(),
  ),
    'fix'	=> array (
        'field_name' => 'fix',
        'name' => 'form[fix]',
        'title' => 'Закрепить',
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
        'default' => 0,
        'sub_type' => 'int',
    ),
    'date_on'	=>
        array (
            'field_name' => 'date_on',
            'name' => 'form[date_on]',
            'title' => 'Дата доступности на сайте',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'default'	=> date('Y-m-d')
        ),
 'name'	=>
  array (
    'field_name' => 'name',
    'name' => 'form[name]',
    'title' => Main::get_lang_str('title', 'db'),
    'must' => 1,
	'style' => 'width:100%',
    'maxlen' => 255,
    'type' => 'textbox',
		'logic' => 'OR',
		'search' => " LIKE '%%%s%%'",
  ),
    'title2' => array (
        'field_name' => 'title2',
        'name' => 'form[title2]',
        'title' => 'Заголовок 2',
        'must' => '0',
        'maxlen' => '500',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '5',
    ),
  'ts'	=>
  array (
    'field_name' => 'ts',
    'name' => 'form[ts]',
    'title' => 'Дата создания',
    'must' => 1,
	'size' => 15,
    'maxlen' => 255,
    'type' => 'hidden',
	'default'	=> date('Y-m-d')
  ),

    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Краткое описание (для списка)',
        'must' => '1',
        'maxlen' => '600',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '10',
    ),
    'more'	=>
        array (
            'field_name' => 'more',
            'name' => 'form[more]',
            'title' => 'ссылка на подробнее',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'more_title' =>
        array (
            'field_name' => 'more_title',
            'name' => 'form[more_title]',
            'title' => 'Текст вместо подробнее',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),

    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => 'Полный текст',
        'must' => '0',
        'maxlen' => '65535',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '20',
        'wysiwyg' => 'tinymce',
    ),
    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => 'Главное изображение для новости',
        'admwidth' => 200,
        'type'	=> 'photo',
        'sub_type'	=> 'photo',
        'newname_func'	=> 'get_file_name()',
        'path'	=> KAT::get_data_link( '/f_news', $dir, KAT_LOOKIG_DATA_DIR ),
        'abspath'	=> KAT::get_data_path('/f_news', $dir, KAT_LOOKIG_DATA_DIR),
    ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
    ),
);