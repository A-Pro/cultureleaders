<?php global  $_CORE;
$FORM_ORDER	= ' ORDER BY ts DESC ';

if (!$_CORE->IS_ADMIN)
    $FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'project_id' =>
        array (

            'field_name' => 'project_id',
            'name' => 'form[project_id]',
            'title' => 'Проект',
            'must' => '0',
            'type' => 'select_from_table',
            'ex_table' => DB_TABLE_PREFIX.'projects',
            'id_ex_table' => 'alias',
            'ex_table_field' => 'name',
            'ex_table_where' => " (hidden != 1 OR hidden IS NULL)",
            'also' => 'class=""',
            'maxlen' => '128',
            'prompt'    => '< Проект >',
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => Main::get_lang_str('title', 'db'),
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),

    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => Main::get_lang_str('add_file', 'db'),
        'admwidth' => 500,
        'type'	=> 'photo',
        'sub_type'	=> 'doc',
        'newname_func'	=> 'get_file_name()',
        'path'	=> KAT::get_data_link( '/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR ),
        'abspath'	=> KAT::get_data_path('/f_'.$_KAT['KUR_ALIAS'], $dir, KAT_LOOKIG_DATA_DIR),
    ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
        'sub_type' => 'int',
    ),
    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default'	=> date('Y-m-d H:i:s')
        ),
    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'Автор',
        'must' => '0',
        'maxlen' => '255',
        'disabeled' => 'true',
        'subtype'   => 'bigint',
        'placeholder' => 'ID Автора',
        'default'	=> $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
    ),

    'from_group' => array (
        'field_name' => 'from_group',
        'name' => 'form[from_group]',
        'title' => 'Группа',
        'must' => '0',
        'maxlen' => '255',
        'placeholder' => 'ID Автора',
        'disabeled' => 'true',
        'default'	=> ($_SESSION['SESS_AUTH']['ALL']['from_group']) ? $_SESSION['SESS_AUTH']['ALL']['from_group'] : $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
        'subtype'   => 'bigint',
    ),
);

// editable access for admin

if ( $_SESSION['SESS_AUTH']['ALL']['from_group'] == $_SESSION['SESS_AUTH']['ID'] || $_CORE->IS_ADMIN) {

    $FORM_DATA['from_auth'] =
        array (
            'field_name' => 'from_auth',
            'name' => 'form[from_auth]',
            'title' => 'Автор',
            'must' => '0',
            'type' => 'select_from_table',
            'ex_table' => DB_TABLE_PREFIX.'auth_pers',
            'id_ex_table' => 'author_id',
            'ex_table_field' => 'author_comment',
            'ex_table_where'  => " from_group = '". $_SESSION['SESS_AUTH']['ALL']['from_group']."' ",
            'also' => 'class=""',
            'maxlen' => '128',
        );

}