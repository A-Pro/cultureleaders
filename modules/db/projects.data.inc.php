<?php
if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; } 

/**
 * name - название строка
 * desc - описание
 * специализации - так же как у persons
 * общее число требуемых специалистов.
 * Контактное лицо - строка
 * email контакта - строка
 * телефон - строка 
 */
global $FORM_FIELD4ALIAS,$_ACCESS,$FORM_ACCESS;
$FORM_WHERE = '';
$FORM_ORDER	= '';
$FORM_FIELD4ALIAS = 'id';

if (!$_CORE->IS_ADMIN)
    $FORM_ORDER = " ORDER by status asc, ts desc ";

if (!$_CORE->IS_ADMIN){
	$FORM_WHERE	= $_ACCESS->get_where( $_KAT['KUR_ALIAS'] );
    $FORM_ACCESS = $FORM_WHERE;
}

// для таблиц админа
// $_KAT[$_KAT['KUR_ALIAS']]['admin_search']	= array( 'add_qualif', 'fedsubject_id', 'from_auth' ); // для db/list
// $_KAT[$_KAT['KUR_ALIAS']]['admin_fields']	= array( 'Доп.' => 'add_qualif', "Суб.Фед." => 'fedsubject_id', 'Польз' => 'from_auth' ); 
/////////////////

/**
 *  FORM_DATA - SHEME INCLUDE
 * **/
include 'projects.sheme.inc.php';


if ($_CORE->IS_ADMIN) {
    $FORM_DATA['from_auth']['type'] = 'select_from_table';
    $FORM_DATA['from_auth']['ex_table'] = DB_TABLE_PREFIX . 'auth_pers';
    $FORM_DATA['from_auth']['id_ex_table'] = 'author_id';
    $FORM_DATA['from_auth']['ex_table_field'] = 'author_login';
    $FORM_DATA['from_auth']['ex_table_where'] = "`type_user` = 'customer'";

}