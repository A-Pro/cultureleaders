<?php global  $_CORE;
$FORM_ORDER = ' ORDER BY ts ASC ';

$FORM_WHERE = " AND project_id = '".$_SESSION['SESS_AUTH']['ALL']['code_project']."'";
$FORM_WHERE .= " AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'project_id' =>
        array (
            'field_name' => 'project_id',
            'name' => 'form[project_id]',
            'title' => 'Проект',
            'must' => '0',
            'maxlen' => 255,
            'type' => 'hidden',
            'default' => $_SESSION['SESS_AUTH']['ALL']['code_project'],
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Наименование файла',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',
        ),

    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => Main::get_lang_str('add_file', 'db'),
        'admwidth' => 500,
        'type'	=> 'photo',
        'sub_type'	=> 'doc',
        'newname_func'	=> 'get_file_name()',
        'maxsize' => 10485760,
        'path'	=> KAT::get_data_link( '/f_docs', $dir, KAT_LOOKIG_DATA_DIR ),
        'abspath'	=> KAT::get_data_path('/f_docs', $dir, KAT_LOOKIG_DATA_DIR),
        'class' => 'btn',
        'example' => '<span class="t-text t-text_xs">Максимально допустимый размер файла: 10 МБ<br>
Файл необходимо загружать в формате .pdf</span>'
    ),
    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default'	=> date('Y-m-d H:i:s')
        ),
    'dt_update' =>
        array (
            'field_name' => 'dt_update',
            'name' => 'form[dt_update]',
            'title' => 'Дата обновления',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default'	=> date('Y-m-d H:i:s')
        ),
    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'Автор',
        'must' => '0',
        'maxlen' => '255',
        'disabeled' => 'true',
        'subtype'   => 'bigint',
        'placeholder' => 'ID Автора',
        'default'	=> $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
    ),

    'from_group' => array (
        'field_name' => 'from_group',
        'name' => 'form[from_group]',
        'title' => 'Группа',
        'must' => '0',
        'maxlen' => '255',
        'placeholder' => 'ID Автора',
        'disabeled' => 'true',
        'default'	=> ($_SESSION['SESS_AUTH']['ALL']['from_group']) ? $_SESSION['SESS_AUTH']['ALL']['from_group'] : $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
        'subtype'   => 'bigint',
    ),
);
