<?php
$agree_eula = intval($_POST['agree_eula']);
$sql = new SQL();
if(isset($_POST['agree_eula']) && $agree_eula == 1 ){
    // пометим что пользователь согласился с политикой конфиденциальности

    $sql->upd(
        DB_TABLE_PREFIX.'auth_pers',
        "`agree_eula` = 1",
        "author_id = '".$_SESSION['SESS_AUTH']['ID']."'");

    $_SESSION['SESS_AUTH']['ALL']['agree_eula'] = 1;
}
// вычислим процент заполненных данных и сохраним в базу
$user_fields = $FORM_DATA;
unset($user_fields['ts'],$user_fields['alias'],$user_fields['author_id']);
$fields = array_keys($user_fields);
$data = $_KAT[$_KAT['KUR_ALIAS']]['last_form'];
$filled = 0;
foreach ($fields as $field){
    if(!empty($data[$field])) $filled ++;
}
$complit = ceil(100 * ($filled/count($fields)));

$res = $sql->upd(
    DB_TABLE_PREFIX.'auth_pers',
    "`complit` = ".$complit,
    "author_id = '".$_SESSION['SESS_AUTH']['ID']."'");

header("Location: /empty/db/accounts/".$_SESSION['SESS_AUTH']['ALL']['alias']);
exit;