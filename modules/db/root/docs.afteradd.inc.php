<?php global $_KAT;
$alias = $Cmd;

$code_project = $_SESSION['SESS_AUTH']['ALL']['code_project'];

if(!empty($alias) && $alias!='add' && isset($_GET['hide'])){
    $data = SQL::getrow(
        '*',
        DB_TABLE_PREFIX.'docs',
        "`alias` = '{$alias}' AND project_id = '{$code_project}' ",
        'LIMIT 1',
        DEBUG);

    // проверку на дату этапа, материалы по прошедшему этапу удалять нельзя
    if(!empty($data['ts'])){
        $del_file = true; // можно удалять
        // закончился этап 1 ?
        if ($_CONF['settings']['stage1_off'] > '0000-00-00'
                                            && date('Y-m-d') > $_CONF['settings']['stage1_off']) {
            $del_file = ($data['ts'] < $_CONF['settings']['stage1_off'])?false:true;

        }
        // закончился этап 2 ?
        if ($del_file && $_CONF['settings']['stage2_off'] > '0000-00-00'
                                            && date('Y-m-d') > $_CONF['settings']['stage2_off']) {
            $del_file = ($data['ts'] < $_CONF['settings']['stage2_off'])?false:true;

        }
        // закончился этап 3 ?
        if ($del_file && date('Y-m-d') > '2019-11-25' /*$_CONF['settings']['stage3_off'] > '0000-00-00'
                                            && date('Y-m-d') > $_CONF['settings']['stage3_off']*/ ) {
            $del_file = ($data['ts'] <= '2019-11-25' /*$_CONF['settings']['stage3_off']*/)?false:true;

        }
        // закончился этап 4 ?
        if ($del_file && date('Y-m-d') > '2019-12-06') {
            $del_file = ($data['ts'] <= '2019-12-06')?false:true;

        }
        // закончился этап 5 ?
        if ($del_file && date('Y-m-d') > '2019-12-10') {
            $del_file = ($data['ts'] <= '2019-12-10')?false:true;

        }
        if($del_file) // удалим
            SQL::del(DB_TABLE_PREFIX.'docs', "`alias` = '{$alias}'", DEBUG);
        else // вернем hidden = 0
            SQL::upd(DB_TABLE_PREFIX.'docs', 'hidden=0',"`alias` = '{$alias}'", DEBUG);
    }
}
if($del_file !== true)
    die($_KAT['ERROR']);
header("Location: /empty/db/projects/".$code_project);
exit;