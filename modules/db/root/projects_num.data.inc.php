<?php
if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }

$FORM_ORDER = " ORDER BY t1.ts desc ";
// просмотр проектов с номинацией как у своего проекта, но не свой проект
$FORM_WHERE = " AND t1.alias != '".$_SESSION['SESS_AUTH']['ALL']['code_project']."'";
$FORM_WHERE .= " AND t1.status > 1 ";
$FORM_WHERE .= " AND t1.cont_org != '' ";
if(!empty($_SESSION['SESS_AUTH']['ALL']['project_nom'])) {
    $FORM_WHERE .= " AND t1.nomination_id = '" . $_SESSION['SESS_AUTH']['ALL']['project_nom'] . "'";
}else{
    $FORM_WHERE = " AND 0 ";
}

$FORM_SELECT_BEFORE = 't1.';
$FORM_SELECT = ', t2.author_comment as leader, t2.region_id as region_id, t3.name as region_name';
$FORM_FROM = ' as t1 LEFT JOIN '.DB_TABLE_PREFIX.'auth_pers as t2 ON t2.code_project = t1.alias AND '.
                't2.role_project = 99';
$FORM_FROM .= ' LEFT JOIN '.DB_TABLE_PREFIX.'regions as t3 ON t2.region_id = t3.id ';
    /**
 *  FORM_DATA - SHEME INCLUDE
 * **/

$FORM_DATA= array (
    'id' => array (
        'field_name' => 'id',
        'name' => 'form[id]',
        'title' => 'id',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default' => strtoupper(uniqid()),
    ),
    'ts' => array	(
        'field_name' =>	'ts',
        'name' =>	'form[ts]',
        'title'	=> 'Дата',
        'must' =>	0,
        'size' =>	15,
        'maxlen' =>	255,
        'type' =>	'hidden',
        'readonly' =>	'true',
        'default'	=> date('Y-m-d H:i')
    ),
    'name' => array (
        'field_name' => 'name',
        'name' => 'form[name]',
        'title' => 'Название',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => 'Название',
        'search'    => " LIKE '%%%s%%' ",
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),

    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Краткое описание',
        'must' => '0',
        'maxlen' => '500',
        'type' => 'textarea',
        'style' => 'width:100%',
        'cols' => '50',
        'rows' => '5',
        'logic' => 'OR',
        'search'    => " LIKE '%%%s%%' ",
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),

    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => 'Полное описание',
        'must' => '0',
        'maxlen' => '65535',
        'type' => 'textarea',
        'style' => 'width:100%',
        'cols' => '50',
        'rows' => '20',
//        'wysiwyg'	=> 'tinymce',
        'logic' => 'OR',
        'search'    => " LIKE '%%%s%%' ",
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),

    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'Автор',
        'must' => '0',
        'maxlen' => '255',
        'disabeled' => 'true',
        'subtype'   => 'bigint',
        'placeholder' => 'ID Автора',
        'default'	=> $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
    ),

    'from_group' => array (
        'field_name' => 'from_group',
        'name' => 'form[from_group]',
        'title' => 'Группа',
        'must' => '0',
        'maxlen' => '255',
        'placeholder' => 'ID Автора',
        'disabeled' => 'true',
        'default'	=> ($_SESSION['SESS_AUTH']['ALL']['from_group']) ? $_SESSION['SESS_AUTH']['ALL']['from_group'] : $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
        'subtype'   => 'bigint',
    ),
);
if(!isset($_GET['edit'])){
    $FORM_DATA['status'] = array (
        'field_name' => 'status',
        'name' => 'form[status]',
        'title' => 'Статус',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'select',
        "arr"   => array(
            0 => '',
            -20 => 'Заблокирован',
            -10 => 'Не допущен',
            1 => 'Ожидает модерации',
            10 => 'Допущен',
            30 => 'На доработку',
            40 => 'Отобран',
            50 => 'Номинирован',
            60 => 'Победитель',
        )
    );
    $FORM_DATA['nomination_id'] = array(
        'field_name' => 'nomination_id',
        'name' => 'form[nomination_id]',
        'title' => 'Номинация',
        'must' => '0',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'nominations',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        //'ex_table_where'  => " from_group = '". $_SESSION['SESS_AUTH']['ALL']['from_group']."' ",
        'also' => 'class=""',
        'maxlen' => '128',
    );
}

