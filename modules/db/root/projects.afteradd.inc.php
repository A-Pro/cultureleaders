<?php

$data = $_KAT[$_KAT['KUR_ALIAS']]['last_form'];
$code_project = $data['alias'];
$agree_normativ = (empty($_SESSION['SESS_AUTH']['ALL']['agree_normativ']))?intval($_POST['agree_normativ']):$_SESSION['SESS_AUTH']['ALL']['agree_normativ'];
$sql = new SQL();

if(!empty($code_project) && ($Cmd == 'add' || empty($_SESSION['SESS_AUTH']['ALL']['agree_normativ'])) && $agree_normativ == 1 ){
    // пропишем для создавшего код проекта статус лидера проекта

    $sql->upd(
        DB_TABLE_PREFIX.'auth_pers',
        "`code_project` = '{$code_project}', `role_project` = '99', `agree_normativ` = '{$agree_normativ}'",
        "author_id = '".$data['from_auth']."'");

    $_SESSION['SESS_AUTH']['ALL']['code_project'] = $code_project;
    $_SESSION['SESS_AUTH']['ALL']['role_project'] = 99;
    $_SESSION['SESS_AUTH']['ALL']['agree_normativ'] = $agree_normativ;
}

// вычислим процент заполненных данных и сохраним в базу
$user_fields = $FORM_DATA;
unset($user_fields['ts'],$user_fields['alias'],$user_fields['id'],$user_fields['from_auth'],$user_fields['from_group'],$user_fields['status']);
$fields = array_keys($user_fields);
$filled = 0;
$project = $sql->getrow("`".implode("`,`",$fields)."`",DB_TABLE_PREFIX.'projects',"alias = '".$code_project."'");
foreach ($fields as $field){
    if(!empty($project[$field])) $filled ++;
}
$complit = ceil(100 * ($filled/count($fields)));

$res = $sql->upd(
    DB_TABLE_PREFIX.'projects',
    "`complit` = ".$complit,
    "alias = '".$code_project."'");

header("Location: /empty/db/projects/".$code_project);
exit;