<?php
if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }

global $FORM_FIELD4ALIAS, $_ACCESS, $FORM_ACCESS;
$FORM_WHERE = '';
$FORM_ORDER	= '';
$FORM_FIELD4ALIAS = 'id';

if (!$_CORE->IS_ADMIN)
    $FORM_ORDER = " ORDER BY ts desc ";
// ограничение на просмотр только к своего проекта (группы)
$FORM_WHERE .= " AND alias = '".$_SESSION['SESS_AUTH']['ALL']['code_project']."'";

/**
 *  FORM_DATA - SHEME INCLUDE
 * **/

$FORM_DATA= array (
    'id' => array (
        'field_name' => 'id',
        'name' => 'form[id]',
        'title' => 'id',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default' => strtoupper(uniqid()),
    ),
    'ts' => array	(
        'field_name' =>	'ts',
        'name' =>	'form[ts]',
        'title'	=> 'Дата',
        'must' =>	0,
        'size' =>	15,
        'maxlen' =>	255,
        'type' =>	'hidden',
        'readonly' =>	'true',
        'default'	=> date('Y-m-d H:i')
    ),
    'name' => array (
        'field_name' => 'name',
        'name' => 'form[name]',
        'title' => 'Название',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),
    'nomination_id' => array(
        'field_name' => 'nomination_id',
        'name' => 'form[nomination_id]',
        'title' => 'Номинация',
        'must' => '0',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'nominations',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'maxlen' => '128',
        'prompt' => '',
    ),

    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Краткое описание',
        'must' => '0',
        'maxlen' => '300',
        'type' => 'textarea',
        'style' => 'width:100%',
        'cols' => '50',
        'rows' => '5',
        'placeholder'=>'до 250 знаков без пробелов',
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),

    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => 'Полное описание',
        'must' => '0',
        'maxlen' => '950',
        'type' => 'textarea',
        'style' => 'width:100%',
        'cols' => '50',
        'rows' => '15',
//        'wysiwyg'	=> 'tinymce',
        'placeholder'=>'до 750 знаков без пробелов',
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
        'example' => '<span class="t-text t-text_xs">
- Опишите суть проекта: идея, формат, содержание<br>
- Для какой аудитории будет работать ваш проект?<br>
- Расскажите о цели создания вашего проекта. Какие результаты вы ожидаете по итогам?<br>
</span><br><br>'
    ),
    'doc' => array(
        'field_name' =>	'doc',	// должно совпадать	с 'name'!!!
        'name'	=> 'doc',
        'title'	=> 'Презентация до первого очного модуля',
        'type'	=> 'photo',
        'sub_type'	=> 'doc',
        'class' => 'btn ',
        'maxsize' => 10485760,
        'newname_func'	=> 'get_file_name("present")',
        'path'	=> '/data/db/f_projects/',
        'abspath'	=> $_SERVER['DOCUMENT_ROOT'].'/data/db/f_projects/',
        'example' => '<span class="t-text t-text_xs">Презентация должна состоять из 5 слайдов: '.'<ol><li>миссия / задача проекта</li> 
<li>описание проекта (суть идеи, регион и т.д.)</li> 
<li>команда</li> 
<li>план реализации</li> 
<li>бизнес-план / количество требуемых инвестиций с разбивкой статей расходов</li></ol>
Максимально допустимый размер файла: 10 МБ</span><br><br>',
    ),/*
    'url_youtube' => array (
        'field_name' => 'url_youtube',
        'name' => 'form[url_youtube]',
        'title' => 'Мотивационное видео письмо',
        'must' => 0,
        'size' => 2,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => 'ссылка на видео (Youtube)',
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
        'example' => '<span class="t-text t-text_xs">Расскажите всей командой проекта, почему вы хотите реализовать вашу идею и как в этом вам может помочь наша программа. Продолжительность видео не более 2 минут</span>',
    ),*/
    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'Автор',
        'must' => '0',
        'maxlen' => '255',
        'disabeled' => 'true',
        'subtype'   => 'bigint',
        'placeholder' => 'ID Автора',
        'default'	=> $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
    ),

    'from_group' => array (
        'field_name' => 'from_group',
        'name' => 'form[from_group]',
        'title' => 'Группа',
        'must' => '0',
        'maxlen' => '255',
        'placeholder' => 'ID Автора',
        'disabeled' => 'true',
        'default'	=> ($_SESSION['SESS_AUTH']['ALL']['from_group']) ? $_SESSION['SESS_AUTH']['ALL']['from_group'] : $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
        'subtype'   => 'bigint',
    ),
);
if(!isset($_GET['edit'])){
    $FORM_DATA['status'] = array (
        'field_name' => 'status',
        'name' => 'form[status]',
        'title' => 'Статус',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'select',
        "arr"   => array(
            0 => '',
            -20 => 'Заблокирован',
            -10 => 'Не допущен',
            1 => 'Ожидает модерации',
            10 => 'Допущен',
            30 => 'На доработку',
            40 => 'Отобран',
            50 => 'Номинирован',
            60 => 'Победитель',
        )
    );
}

