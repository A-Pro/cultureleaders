<?php global  $_CORE, $FORM_FIELD4ALIAS, $_KAT;
$FORM_FIELD4ALIAS = 'alias';
$FORM_FROM = '';
$FORM_ORDER	= " ORDER BY ".$_KAT['KUR_TABLE'].".hidden ASC, ".$_KAT['KUR_TABLE'].".ts DESC ";
$FORM_WHERE = '';
if (!$_CORE->IS_ADMIN)
    $FORM_WHERE	= " AND (".$_KAT['KUR_TABLE'].".hidden != 1 OR ".$_KAT['KUR_TABLE'].".hidden IS NULL)";
if($_SESSION['SESS_AUTH']['ALL']['account_type'] == 0){
    $FORM_WHERE	= " AND ".$_KAT['KUR_TABLE'].".status_project <= '".$_SESSION['status_project']."' ";
}

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Название документа',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),

    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => Main::get_lang_str('add_file', 'db'),
        'admwidth' => 500,
        'type'	=> 'photo',
        'sub_type'	=> 'doc',
        'newname_func'	=> 'get_file_name()',
        'path'	=> KAT::get_data_link( '/f_manuals', $dir, KAT_LOOKIG_DATA_DIR ),
        'abspath'	=> KAT::get_data_path('/f_manuals', $dir, KAT_LOOKIG_DATA_DIR),
    ),

    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Краткое описание',
        'must' => '1',
        'maxlen' => '600',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '5',
    ),
    'status_project' => array(
        'field_name' => 'status_project',
        'name' => 'form[status_project]',
        'title' => 'Доступно проектам со статусом',
        'must' => '0',
        'type' => 'select',
        "arr"   => array(
            '' => 'любой статус',
            40 => 'Отобран',
            50 => 'Номинирован',
            60 => 'Победитель',
        ),
        'also' => 'class=""',
        'maxlen' => '128',
    ),
    'project_id' => array(
        'field_name' => 'project_id',
        'name' => 'form[project_id]',
        'title' => 'Доступ для участников проекта',
        'must' => '0',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'projects',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        //'ex_table_where'  => " from_group = '". $_SESSION['SESS_AUTH']['ALL']['from_group']."' ",
        'also' => 'class=""',
        'maxlen' => '128',
        'prompt' => '...'
    ),
    'nomination_id' => array(
        'field_name' => 'nomination_id',
        'name' => 'form[nomination_id]',
        'title' => 'Доступ для участников номинации',
        'must' => '0',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'nominations',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        //'ex_table_where'  => " from_group = '". $_SESSION['SESS_AUTH']['ALL']['from_group']."' ",
        'also' => 'class=""',
        'maxlen' => '128',
        'prompt' => '...'
    ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
        'sub_type' => int
    ),
    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default'	=> date('Y-m-d H:i:s')
        ),
    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'Автор',
        'must' => '0',
        'maxlen' => '255',
        'disabeled' => 'true',
        'subtype'   => 'bigint',
        'placeholder' => 'ID Автора',
        'default'	=> $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
    ),

    'from_group' => array (
        'field_name' => 'from_group',
        'name' => 'form[from_group]',
        'title' => 'Группа',
        'must' => '0',
        'maxlen' => '255',
        'placeholder' => 'ID Автора',
        'disabeled' => 'true',
        'default'	=> ($_SESSION['SESS_AUTH']['ALL']['from_group']) ? $_SESSION['SESS_AUTH']['ALL']['from_group'] : $_SESSION['SESS_AUTH']['ID'],
        'type' => 'hidden',
        'subtype'   => 'bigint',
    ),
);
if(!isset($_GET['edit'])){
    $FORM_DATA['status_project']['title'] = 'Статус проекта';
    $FORM_DATA['project_id']['title'] = 'Проект';
    $FORM_DATA['nomination_id']['title'] = 'Номинация';
}
