<?php global  $_CORE, $FORM_WHERE, $FORM_ORDER,$FORM_FIELD4ALIAS;
$FORM_ORDER	= ' ORDER BY name ASC ';
$FORM_WHERE = '';
if (!$_CORE->CURR_TPL )
    $FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";
$FORM_FIELD4ALIAS = 'alias';
$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => Main::get_lang_str('title', 'db'),
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'url' => array (
        'field_name' => 'url',
        'name' => 'form[url]',
        'title' => 'Ссылка на страницу партнера',
        'must' => '1',
        'maxlen' => '255',
        'type' => 'textbox',
        'style' => 'width:100%',

    ),
    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => 'логотип',
        'admwidth' => 200,
        'type'	=> 'photo',
        'sub_type'	=> 'photo',
        'newname_func'	=> 'get_file_name()',
        'path'	=> KAT::get_data_link( '/f_partners', $dir, KAT_LOOKIG_DATA_DIR ),
        'abspath'	=> KAT::get_data_path('/f_partners', $dir, KAT_LOOKIG_DATA_DIR),
    ),
    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => 'Дата создания',
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'default'	=> date('Y-m-d H:i:s')
        ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
        'sub_type' => 'int'
    ),
);