<?php global  $_CORE, $FORM_WHERE, $FORM_ORDER,$FORM_FIELD4ALIAS;
$FORM_ORDER	= ' ORDER BY prior ASC, id ASC';
$FORM_WHERE = '';
if (!$_CORE->IS_ADMIN )
    $FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL) ";
$FORM_FIELD4ALIAS = 'alias';
$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Заголовок',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'ts'	=>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => 'Дата создания',
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'default'	=> date('Y-m-d')
        ),

    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => 'Текст',
        'must' => '1',
        'maxlen' => '600',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '3',
    ),

    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
    ),

    'prior'	=> array (
        'field_name' => 'prior',
        'name' => 'form[prior]',
        'title' => 'Очередность',
        'must' => 0,
        'type' => 'hidden',
        'sub_type' => 'int',
        'default' => 999
    ),
);