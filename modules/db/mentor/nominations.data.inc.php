<?php global  $_CORE;
$FORM_ORDER	= ' ORDER BY id DESC ';

$FORM_WHERE	= "id = {$_SESSION['SESS_AUTH']['ALL']['nomination_id']} AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),

    'notification' => array (
        'field_name' => 'notification',
        'name' => 'form[notification]',
        'title' => 'Текст объявления',
        'must' => '0',
        'maxlen' => '1200',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '5',
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
);