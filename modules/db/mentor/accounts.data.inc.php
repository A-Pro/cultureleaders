<?php
global $_CORE,$FORM_FIELD4ALIAS, $_ACCESS, $_CONF;

if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }


$FORM_WHERE = " AND `author_id` = {$_SESSION['SESS_AUTH']['ID']}";

$FORM_WHERE	.= " AND `account_type` = ".ACCTYPE_MENTOR." ";

$FORM_ORDER	= '';
$FORM_FIELD4ALIAS = 'alias';

$FORM_DATA= array (
    'author_id' => array (
        'field_name' => 'author_id',
        'name' => 'form[author_id]',
        'title' => '',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default'   => time(),
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'ts' => array	(
        'field_name' =>	'ts',
        'name' =>	'form[ts]',
        'title'	=> 'Дата',
        'must' =>	0,
        'size' =>	15,
        'maxlen' =>	255,
        'type' =>	'hidden',
        'readonly' =>	'true',
        'default'	=> date('Y-m-d H:i')
    ),
    'doc' => array(
        'field_name' =>	'doc',	// должно совпадать	с 'name'!!!
        'name'	=> 'doc',
        'title'	=> 'Фото',
        'maxwidth' => 4000,
        'maxheight'	=> 2000,
        'admwidth'	=> 150,
        'type'	=> 'photo',
        'sub_type'	=> 'photo',
        'class' => 'btn ',
        'newname_func'	=> 'get_file_name("ava")',
        'path'	=> '/data/db/f_accounts/',
        'abspath'	=> $_SERVER['DOCUMENT_ROOT'].'/data/db/f_accounts/',
    ),
    'author_comment' => array (
        'field_name' => 'author_comment',
        'name' => 'form[author_comment]',
        'title' => 'ФИО',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
);
