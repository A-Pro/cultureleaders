<?php global $_CORE, $FORM_FIELD4ALIAS;
$FORM_ORDER	= ' ORDER BY prior ASC, name ASC ';
$FORM_FIELD4ALIAS = 'alias';
if (!$_CORE->IS_ADMIN)
    $FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'ФИО',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'position'	=>
        array (
            'field_name' => 'position',
            'name' => 'form[position]',
            'title' => 'Должность',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),

    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => Main::get_lang_str('add_file', 'db'),
        'admwidth' => 200,
        'type'	=> 'photo',
        'sub_type'	=> 'photo',
        'newname_func'	=> 'get_file_name()',
        'path'	=> KAT::get_data_link( '/f_teachers', $dir, KAT_LOOKIG_DATA_DIR ),
        'abspath'	=> KAT::get_data_path('/f_teachers', $dir, KAT_LOOKIG_DATA_DIR),
    ),
    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => Main::get_lang_str('about', 'db'),
        'must' => '0',
        'maxlen' => '3000',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '20',
//        'wysiwyg'	=> 'tinymce',
        'logic' => 'OR',
        'search' => " LIKE '%%%s%%'",
    ),
    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default'	=> date('Y-m-d')
        ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
    ),
    'prior' => array (
        'field_name' 	=> 'prior',
        'name' 			=> 'form[prior]',
        'title'			=> 'сортировка',
        'must' 			=> 0,
        'maxlen' 		=> 20,
        'type' 			=>'hidden',
        'sub_type' 		=> 'int'
    )
);