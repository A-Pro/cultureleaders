<?php
global $_CORE,  $_ACCESS, $_CONF, $FORM_FIELD4ALIAS;

if (!$_CORE->IS_ADMIN || empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }
$FORM_FIELD4ALIAS = 'alias';

$FORM_WHERE = '';
$FORM_ORDER	= '';

if (!$_CORE->IS_ADMIN)
    $FORM_ORDER = " ORDER by author_comment asc, ts desc ";

if (!$_CORE->IS_ADMIN)
	$FORM_WHERE	= $_ACCESS->get_where( $_KAT['KUR_ALIAS'] ); //" AND (from_auth = '".$_SESSION['SESS_AUTH']['ID']."' " . $FROM_WHERE_ACCESS . ") AND (hidden != 1 OR hidden IS NULL)";

// для таблиц админа
// $_KAT[$_KAT['KUR_ALIAS']]['admin_search']	= array( 'add_qualif', 'fedsubject_id', 'from_auth' ); // для db/list
// $_KAT[$_KAT['KUR_ALIAS']]['admin_fields']	= array( 'Доп.' => 'add_qualif', "Суб.Фед." => 'fedsubject_id', 'Польз' => 'from_auth' ); 
/////////////////

$FORM_DATA= array (
    'author_id' => array (
        'field_name' => 'author_id',
        'name' => 'form[author_id]',
        'title' => '',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default'   => time(),
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'ts' => array	(
        'field_name' =>	'ts',
        'name' =>	'form[ts]',
        'title'	=> 'Дата',
        'must' =>	0,
        'size' =>	15,
        'maxlen' =>	255,
        'type' =>	'hidden',
        'readonly' =>	'true',
        'default'	=> date('Y-m-d H:i')
    ),
    'hidden' => array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => 'Скрыто',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'class' => 'form-control'
    ),
    'author_login' => array (
        'field_name' => 'author_login',
        'name' => 'form[author_login]',
        'title' => 'E-mail',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => 'логин',
        'search'    => " LIKE '%%%s%%' ",
    ),
    'author_comment' => array (
        'field_name' => 'author_comment',
        'name' => 'form[author_comment]',
        'title' => 'ФИО',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'search'    => " LIKE '%%%s%%' ",
    ),
    'author_surname' => array (
        'field_name' => 'author_surname',
        'name' => 'form[author_surname]',
        'title' => 'Фамилия',
        'must' => 1,
        'also' => 'required',
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'author_patronymic' => array (
        'field_name' => 'author_patronymic',
        'name' => 'form[author_patronymic]',
        'title' => 'Отчество',
        'must' => 1,
        'also' => 'required',
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'account_type' => array (
        'field_name' => 'account_type',
        'name' => 'form[account_type]',
        'title' => 'Доступ',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'select',
        "arr"   => $_CONF['account_type'],
        'placeholder'   => '',
        'search'    => " LIKE '%%%s%%' ",
    ),/*
    'author_passwd' =>
    array (
        'field_name' => 'author_passwd',
        'name' => 'form[author_passwd]',
        'title' => 'Пароль',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'password',
        'style' => 'width:100%',
        'also'  => ' data-toggle="password" autocomplete="new-password" 
       onblur="this.setAttribute(\'readonly\', \'readonly\');" 
       onfocus="this.removeAttribute(\'readonly\');" readonly ',
        'placeholder' => '<пароль>',
    ),*/
    'code_project' => array (
        'field_name' => 'code_project',
        'name' => 'form[code_project]',
        'title' => 'Код проекта',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
    ),
    'role_project' => array (
        'field_name' => 'role_project',
        'name' => 'form[role_project]',
        'title' => 'Роль в проекте',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'select',
        "arr"   => $_CONF['role_project']
    ),
        /*
	'cont' => array (
		'field_name' => 'cont',
		'name' => 'form[cont]',
		'title' => 'Ремарка',
		'must' => '0',
		'maxlen' => '65535',
		'type' => 'textbox',
		'style' => 'width:100%',
        'logic' => 'OR',
        'search'    => " LIKE '%%%s%%' ",
	),*/
    'from_auth' => array (
        'field_name' => 'from_auth',
        'name' => 'form[from_auth]',
        'title' => 'From Auth',
        'must' => '0',
        'maxlen' => '255',
		'placeholder' => 'ID Автора',
		'default'	=> $_SESSION['SESS_AUTH']['ALL']['from_auth'],
        'readonly'  => true,
        'type' => 'hidden',
    ),
  'hidden' => 
    array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => 'Скрыть?',
        'must' => 0,
        'maxlen' => 20,
        'default' => '0',
        'type' => 'hidden',
    ),
    
);

if ($_CORE->IS_ADMIN) {
	$FORM_DATA['from_auth']['type'] = 'select_from_table';
	$FORM_DATA['from_auth']['ex_table'] = DB_TABLE_PREFIX.'auth_pers';
	$FORM_DATA['from_auth']['id_ex_table'] = 'author_id';
	$FORM_DATA['from_auth']['ex_table_field'] = 'author_login';
	$FORM_DATA['from_auth']['ex_table_where'] = "`type_user` = 'customer'";
	
    $FORM_DATA['from_auth']['also'] = '';
    
    $FORM_DATA['hidden']['type'] = 'checkbox';
}

?>