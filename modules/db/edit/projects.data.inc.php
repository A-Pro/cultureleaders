<?php
if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }

global $FORM_FIELD4ALIAS, $_ACCESS, $FORM_ACCESS;
$FORM_WHERE = '';
$FORM_ORDER	= '';
$FORM_FIELD4ALIAS = 'id';

if (!$_CORE->IS_ADMIN)
    $FORM_ORDER = " ORDER BY ts desc ";


/**
 *  FORM_DATA - SHEME INCLUDE
 * **/

$FORM_DATA= array (
    'id' => array (
        'field_name' => 'id',
        'name' => 'form[id]',
        'title' => 'id',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default' => strtoupper(uniqid()),
    ),
    'ts' => array	(
        'field_name' =>	'ts',
        'name' =>	'form[ts]',
        'title'	=> 'Дата',
        'must' =>	0,
        'size' =>	15,
        'maxlen' =>	255,
        'type' =>	'hidden',
        'readonly' =>	'true',
        'default'	=> date('Y-m-d H:i')
    ),
    'name' => array (
        'field_name' => 'name',
        'name' => 'form[name]',
        'title' => 'Название',
        'type' => 'view_value',
    ),

    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Краткое описание',
        'type' => 'view_value',
    ),

    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => 'Полное описание',
        'type' => 'view_value',
    ),
    'doc' => array(
        'field_name' =>	'doc',	// должно совпадать	с 'name'!!!
        'name'	=> 'doc',
        'title'	=> 'Презентация до первого очного модуля',
        'type' => 'get_code',
        'get_code'  => 'show_url($this->global_val("form[doc]"), "открыть в новом окне","/data/db/f_projects/")',
        'exec_code' => 'true',
    ),/*
    'url_youtube' => array (
        'field_name' => 'url_youtube',
        'name' => 'form[url_youtube]',
        'title' => 'Мотивационное видео письмо',
        'type' => 'get_code',
        'get_code'  => 'show_url($this->global_val("form[url_youtube]"), "открыть в новом окне")',
        'exec_code' => 'true',
    ),*/
);

$FORM_DATA['group_project'] = array(
        'must' => '0',
        'name'      => 'form[group_project]',
        'type'      => 'get_code',
        'title'     => 'Участники команды ',
        'readonly'  => 'true',
        'get_code'  => 'project_group($this->global_val("form[alias]"),true)',
        'exec_code'  => 'true',
    );

// разбивка на стадии в соответствии с датами из конфигуратора дат
$FORM_DATA['docs_project1'] = array(
    'must'      => '0',
    'name'      => 'form[docs_project1]',
    'type'      => 'get_code',
    'title'     => 'Файлы (1-я стадия)<br>до '.$_CONF['settings']['stage1_off'],
    'get_code'  => 'project_docs($this->global_val("form[alias]"),1)',
    'exec_code' => 'true',
    'readonly'  => 'true',
);

if ($_CONF['settings']['stage1_off'] > '0000-00-00') {
    // идет этап 1 ?
    if ( date('Y-m-d') > $_CONF['settings']['stage1_off']) {
        $FORM_DATA['docs_project2'] = array(
            'must'      => '0',
            'name'      => 'form[docs_project2]',
            'type'      => 'get_code',
            'title'     => 'Файлы (2-я стадия)<br>с '.$_CONF['settings']['stage1_off'].' по '.$_CONF['settings']['stage2_off'],
            'get_code'  => 'project_docs($this->global_val("form[alias]"),2)',
            'exec_code' => 'true',
            'readonly'  => 'true',
        );
    }
}
// закончился этап 2 ?
if ($_CONF['settings']['stage2_off'] > '0000-00-00') {
    if ( date('Y-m-d') > $_CONF['settings']['stage2_off']) {
        $FORM_DATA['docs_project3'] = array(
            'must'      => '0',
            'name'      => 'form[docs_project3]',
            'type'      => 'get_code',
            'title'     => 'Файлы (3-я стадия)<br>с '.$_CONF['settings']['stage2_off'],
            'get_code'  => 'project_docs($this->global_val("form[alias]"),3)',
            'exec_code' => 'true',
            'readonly'  => 'true',
        );
    }
}

$FORM_DATA['best'] = array (
        'field_name' => 'best',
        'name' => 'form[best]',
        'title' => 'Лучший проект',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'checkbox',
        'default' => 0,
        'sub_type' => 'int',
    );

$FORM_DATA['cont_org'] = array (
        'field_name' => 'cont_org',
        'name' => 'form[cont_org]',
        'title' => 'Описание редактора',
        'must' => '0',
        'maxlen' => '65535',
        'type' => 'textarea',
        'style' => 'width:100%',
        'cols' => '50',
        'rows' => '5',
//        'wysiwyg'	=> 'tinymce',
        'logic' => 'OR',
        'search'    => " LIKE '%%%s%%' ",
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    );

