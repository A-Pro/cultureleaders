<?php global  $_CORE;
$FORM_ORDER	= ' ORDER BY id DESC ';

if (!$_CORE->IS_ADMIN)
    $FORM_WHERE	= "AND (hidden != 1 OR hidden IS NULL)";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Название',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'default'	=> date('Y-m-d H:i:s')
        ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
        'sub_type' => 'int'
    ),
);