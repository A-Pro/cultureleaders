<?php
unset($_KAT['ADM_MENU']);
$_KAT['ADM_MENU']["/db/members/"] = array("/ico/a_tree.gif", "Список участников", "db/members/");
$_KAT['ADM_MENU']["/db/users_role/"] = array("/ico/a_tree.gif", "Роли пользователей", "db/users_role/");
$_KAT['ADM_MENU']["/db/projects/"] = array("/ico/a_tree.gif", "Список проектов", "db/projects/");
$_KAT['ADM_MENU']["/db/manuals/"] = array("/ico/a_tree.gif", "Методические материалы", "db/manuals/");
$_KAT['ADM_MENU']["/db/nominations/"] = array("/ico/a_tree.gif", "Номинации", "db/nominations/");
$_KAT['ADM_MENU']["/db/events/"] = array("/ico/a_tree.gif", "Анонс лекций", "db/events/");
$_KAT['ADM_MENU']["/db/events_subscribe/"] = array("/ico/a_tree.gif", "Зарегистрированные на лекции", "db/events_subscribe/");
$_KAT['ADM_MENU']["/db/lectures/"] = array("/ico/a_tree.gif", "Курс лекций", "db/lectures/");
$_KAT['ADM_MENU']["/db/translations/"] = array("/ico/a_tree.gif", "Трансляции", "db/translations/");
$_KAT['ADM_MENU']["/db/obsv/"] = array("/ico/a_tree.gif", "Вопросы с сайта", "db/obsv/");
$_KAT['ADM_MENU']["/db/notes/"] = array("/ico/a_tree.gif", "Записки на главной", "db/notes/");
$_KAT['ADM_MENU']["/db/config/default?edit"] = array("/ico/a_tree.gif", "Конфигуратор дат", "/db/config/default?edit");
/*$_KAT['ADM_MENU']["/db/news/"] = array("/ico/a_tree.gif", "Новости", "db/news/");
$_KAT['ADM_MENU']["/db/mentors/"] = array("/ico/a_tree.gif", "Менторы", "db/mentors/");
$_KAT['ADM_MENU']["/db/experts/"] = array("/ico/a_tree.gif", "Эксперты", "db/experts/");
$_KAT['ADM_MENU']["/db/teachers/"] = array("/ico/a_tree.gif", "Преподаватели", "db/teachers/");*/


/**** ACCOUNTS **********/
$_KAT['onpage_def']['accounts']	= 50;
$_KAT['TABLES']['accounts']     = 'auth_pers';
$_KAT['SRC']['accounts']        = 'accounts';
$_KAT['TITLES']['accounts']     = 'Список участников';
$_KAT['FILES']['accounts']      = 'org/accounts.data.inc.php';
$_KAT['accounts']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['accounts']   = '/tmp/inside';
$_KAT['accounts']['NOSEO']      = true;
$_KAT['INDEX']['accounts']      = 'author_id';
$_KAT['accounts']['adm_fields'] = array('author_comment','author_login','code_project', 'role_project','complit');
$_KAT['accounts']['admin_search'] = array('author_comment','author_surname','author_login','region_id','code_project', 'role_project');
$_KAT['accounts']['acc_history'] = true;
$_KAT['accounts']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/org/accounts.afteradd.inc.php';

// по-умолчанию на все запрет
$_KAT['accounts']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END ACCOUNTS **********/

/**** MEMBERS - VIEW **********/
$_KAT['onpage_def']['members'] = 50;
$_KAT['TABLES']['members']     = 'accounts';
$_KAT['SRC']['members']        = 'members';
$_KAT['TITLES']['members']     = 'Список участников';
$_KAT['FILES']['members']      = 'org/members.data.inc.php';
$_KAT['members']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['members']   = '/tmp/inside';
$_KAT['members']['NOSEO']      = true;
$_KAT['INDEX']['members']      = 'author_id';
$_KAT['members']['adm_fields'] = array('author_comment','author_surname','author_login','code_project','status_project', 'role_project','complit');
$_KAT['members']['admin_search'] = array('author_comment','author_surname','author_login','region_id','code_project','status_project', 'role_project','gender','author_age','education');

// по-умолчанию на все запрет
$_KAT['members']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '1', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '1', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END MEMBERS **********/



/**** USERS_ROLE **********/
$_KAT['onpage_def']['users_role']	= 50;
$_KAT['TABLES']['users_role']     = 'auth_pers';
$_KAT['SRC']['users_role']        = 'users_mentors';
$_KAT['TITLES']['users_role']     = 'Роли пользователей';
$_KAT['FILES']['users_role']      = 'org/users_role.data.inc.php';
$_KAT['users_role']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['users_role']   = '/tmp/inside';
$_KAT['users_role']['NOSEO']      = true;
$_KAT['INDEX']['users_role']      = 'author_id';
$_KAT['users_role']['adm_fields'] = array('author_comment','author_login','account_type','nomination_id');
$_KAT['users_role']['admin_search'] = array('author_comment','author_login','account_type','nomination_id');
$_KAT['users_role']['acc_history'] = true;
$_KAT['users_role']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/org/accounts.afteradd.inc.php';

// по-умолчанию на все запрет
$_KAT['users_role']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END USERS_ROLE **********/


/**** PROJECTS **********/
$_KAT['projects']['NOSEO']      = true;
$_KAT['onpage_def']['projects']	= 50;
$_KAT['TABLES']['projects']		= 'projects';
$_KAT['TITLES']['projects']		= 'Список проектов';
$_KAT['projects']['AFTER_ADD']	= 'db/projects/';
$_KAT['FILES']['projects']		= 'org/projects.data.inc.php';
$_KAT['projects']['hidden']		= 'true'; // для db/list
$_KAT['DOC_PATH']['projects']		= '/tmp/inside';
//$_KAT['projects']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/org/projects.afteradd.inc.php';
$_KAT['INDEX']['projects'] = 'alias';
$_KAT['projects']['acc_history'] = true;
$_KAT['projects']['adm_fields'] = array('name','anons','complit','status');
$_KAT['projects']['admin_search'] = array('name','nomination_id','status','complit');

// по-умолчанию на все запрет
$_KAT['projects']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);

/**** END PROJECTS **********/


/**** MANUALS **********/
$_KAT['onpage_def']['manuals'] = 50;
$_KAT['manuals']['adm_fields'] = array('name','anons','hidden');

// по-умолчанию на все запрет
$_KAT['manuals']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END MANUALS **********/

/**** FAQ **********/
$_KAT['onpage_def']['faq'] = 50;

// по-умолчанию на все запрет
$_KAT['faq']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END FAQ **********/

/**** LECTURES **********/
$_KAT['TABLES']['lectures']     = 'lectures';
$_KAT['SRC']['lectures']        = 'lectures';
$_KAT['TITLES']['lectures']     = 'Курс лекций';
$_KAT['FILES']['lectures']      = 'org/lectures.data.inc.php';
$_KAT['lectures']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['lectures']   = '/tmp/inside';
$_KAT['lectures']['NOSEO']      = true;
$_KAT['onpage_def']['lectures'] = 20;
$_KAT['lectures']['acc_history'] = true;
$_KAT['INDEX']['lectures']      = 'alias';

// по-умолчанию на все запрет
$_KAT['lectures']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END LECTURES **********/


/**** NOTES **********/
$_KAT['onpage_def']['notes'] = 20;

// по-умолчанию на все запрет
$_KAT['notes']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END NOTES **********/

/**** TRANSLATIONS **********/
$_KAT['TABLES']['translations']     = 'translations';
$_KAT['SRC']['translations']        = 'translations';
$_KAT['TITLES']['translations']     = 'Трансляции';
$_KAT['FILES']['translations']      = 'org/translations.data.inc.php';
$_KAT['translations']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['translations']   = '/tmp/inside';
$_KAT['translations']['NOSEO']      = true;
$_KAT['onpage_def']['translations'] = 20;
$_KAT['translations']['acc_history'] = true;
$_KAT['INDEX']['translations']      = 'alias';
$_KAT['translations']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/org/translations.afteradd.inc.php';
$_KAT['translations']['adm_fields'] = array('name','anons','online','ts','hidden');

$_KAT['translations']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END TRANSLATIONS **********/


/**** NOMINATIONS **********/
$_KAT['TABLES']['nominations']     = 'nominations';
$_KAT['SRC']['nominations']        = 'nominations';
$_KAT['TITLES']['nominations']     = 'Номинации';
$_KAT['FILES']['nominations']      = 'nominations.data.inc.php';
$_KAT['nominations']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['nominations']   = '/tmp/inside';
$_KAT['nominations']['NOSEO']      = true;
$_KAT['onpage_def']['nominations'] = 20;
$_KAT['nominations']['acc_history'] = true;
$_KAT['INDEX']['nominations']      = 'alias';

// по-умолчанию на все запрет
$_KAT['nominations']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END NOMINATIONS **********/

/**** EVENTS **********/
$_KAT['TABLES']['events']       = 'events';
$_KAT['SRC']['events']          = 'events';
$_KAT['TITLES']['events']       = 'Анонс лекций';
$_KAT['FILES']['events']        = 'org/events.data.inc.php';
$_KAT['events']['hidden']       = 'true'; // для db/list
$_KAT['DOC_PATH']['events']     = '/tmp/inside';
$_KAT['events']['NOSEO']        = true;
$_KAT['INDEX']['events']        = 'alias';
$_KAT['onpage_def']['events']   = 20;
$_KAT['events']['adm_fields'] = array('name','event_date', 'event_time','hidden');
$_KAT['events']['acc_history']  = true;

$_KAT['events']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END EVENTS **********/

/**** CONFIG **********/
$_KAT['config']['NOSEO']        = true;
$_KAT['onpage_def']['config']   = 1;
$_KAT['TABLES']['config']       = 'settings';
$_KAT['TITLES']['config']       = 'Конфигуратор дат';
$_KAT['config']['AFTER_ADD']    = 'db/config/default?edit';
$_KAT['FILES']['config']        = 'org/settings.data.inc.php';
$_KAT['config']['hidden']       = 'true'; // для db/list
$_KAT['DOC_PATH']['config']     = '/tmp/inside';
$_KAT['INDEX']['config']        = 'alias';
$_KAT['config']['acc_history']  = true;


$_KAT['config']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);

/**** END CONFIG **********/

/**** OBSV **********/
$_KAT['onpage_def']['obsv']	= 50;
$_KAT['TABLES']['obsv']     = 'obsv';
$_KAT['SRC']['obsv']        = 'obsv';
$_KAT['TITLES']['obsv']     = 'Вопросы';
$_KAT['FILES']['obsv']      = 'org/obsv.data.inc.php';
$_KAT['obsv']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['obsv']   = '/tmp/inside';
$_KAT['INDEX']['obsv']        = 'alias';
$_KAT['obsv']['NOSEO']      = true;
$_KAT['obsv']['adm_fields'] = array('name','cont','hidden');

$_KAT['obsv']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END OBSV **********/


/**** EVENTS_SUBSCRIBE **********/
$_KAT['onpage_def']['events_subscribe']	= 200;
$_KAT['TABLES']['events_subscribe']     = 'events_subscribe';
$_KAT['SRC']['events_subscribe']        = 'events_subscribe';
$_KAT['TITLES']['events_subscribe']     = 'Зарегистрированные на лекции';
$_KAT['FILES']['events_subscribe']      = 'org/events_subscribe.data.inc.php';
$_KAT['events_subscribe']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['events_subscribe']   = '/tmp/inside';
$_KAT['events_subscribe']['NOSEO']      = true;
$_KAT['events_subscribe']['adm_fields'] = array('name','surname','email','event_id');
$_KAT['events_subscribe']['admin_search'] = array('event_id','email');

// по-умолчанию на все запрет
$_KAT['events_subscribe']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '1', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '1', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END EVENTS_SUBSCRIBE **********/

/**** Доступы к каталогам *****/
$_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']] = array(
    'accounts','projects','manuals','nominations','lectures','translations','config','users_role','obsv','events','faq', 'events_subscribe', 'notes', 'members'
);


/***** добавим частное разрешение на определенный email ******/
if( in_array($_SESSION['SESS_AUTH']['LOGIN'], array('anna.terkina@rsv.ru','organizator','developer') ) ){
    $_KAT['accounts']['EXPORT']     = array('author_login');
    $_KAT['members']['EXPORT']     = array('author_comment','author_surname','author_patronymic','author_login','author_phone','region','city','name_project','gender','author_age','education','achievement','realized_projects','motivation_letter');
    $_KAT['events_subscribe']['EXPORT']     = array('email');
    $_KAT['projects']['EXPORT']     = array('alias','name','anons','cont','nomination_id');
}