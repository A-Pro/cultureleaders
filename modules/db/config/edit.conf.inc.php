<?php
unset($_KAT['ADM_MENU']);
$_KAT['ADM_MENU']["/db/projects/"] = array("/ico/a_tree.gif", "Список проектов", "db/projects/");
$_KAT['ADM_MENU']["/db/news/"] = array("/ico/a_tree.gif", "Новости", "db/news/");
$_KAT['ADM_MENU']["/db/mentors/"] = array("/ico/a_tree.gif", "Менторы", "db/mentors/");
$_KAT['ADM_MENU']["/db/experts/"] = array("/ico/a_tree.gif", "Эксперты", "db/experts/");
$_KAT['ADM_MENU']["/db/teachers/"] = array("/ico/a_tree.gif", "Преподаватели", "db/teachers/");
$_KAT['ADM_MENU']["/db/manuals/"] = array("/ico/a_tree.gif", "Методические материалы", "db/manuals/");
$_KAT['ADM_MENU']["/db/events/"] = array("/ico/a_tree.gif", "Анонс лекций", "db/events/");
$_KAT['ADM_MENU']["/db/events_subscribe/"] = array("/ico/a_tree.gif", "Зарегистрированные на лекции", "db/events_subscribe/");
$_KAT['ADM_MENU']["/db/translations/"] = array("/ico/a_tree.gif", "Трансляции", "db/translations/");
$_KAT['ADM_MENU']["/db/faq/"] = array("/ico/a_tree.gif", "Вопросы", "db/faq/");
$_KAT['ADM_MENU']["/db/notes/"] = array("/ico/a_tree.gif", "Записки на главной", "db/notes/");
$_KAT['ADM_MENU']["/db/partners/"] = array("/ico/a_tree.gif", "Инфо-партнеры", "db/partners/");


/**** PROJECTS **********/
$_KAT['projects']['NOSEO'] = true;
$_KAT['onpage_def']['projects']	= 50;
$_KAT['TABLES']['projects']		= 'projects';
$_KAT['TITLES']['projects']		= 'Список проектов';
$_KAT['projects']['AFTER_ADD']	= 'db/projects/';
$_KAT['FILES']['projects']		= 'edit/projects.data.inc.php';
$_KAT['projects']['hidden']		= 'true'; // для db/list
$_KAT['DOC_PATH']['projects']		= '/tmp/inside';
//$_KAT['projects']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/edit/projects.afteradd.inc.php';
$_KAT['INDEX']['projects'] = 'alias';
$_KAT['projects']['acc_history'] = true;
$_KAT['projects']['adm_fields'] = array('name','anons',);

// по-умолчанию на все запрет
$_KAT['projects']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);

/**** END PROJECTS **********/

/**** NEWS **************/
$_KAT['news']['adm_fields'] = array('name','anons','date_on','ts','hidden');

$_KAT['news']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END NEWS **********/


/**** NOTES **********/
$_KAT['onpage_def']['notes'] = 20;

// по-умолчанию на все запрет
$_KAT['notes']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END NOTES **********/

$_KAT['TABLES']['experts']         = 'experts';
$_KAT['SRC']['experts']            = 'experts';
$_KAT['TITLES']['experts']         = 'Эксперты';
$_KAT['FILES']['experts']          = 'experts.data.inc.php';
$_KAT['experts']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['experts']       = '/tmp/inside';
$_KAT['experts']['NOSEO']          = true;
$_KAT['onpage_def']['experts']     = 20;
$_KAT['experts']['acc_history']    = true;
$_KAT['INDEX']['experts']          = 'id';

$_KAT['TABLES']['mentors']         = 'mentors';
$_KAT['SRC']['mentors']            = 'mentors';
$_KAT['TITLES']['mentors']         = 'Менторы';
$_KAT['FILES']['mentors']          = 'mentors.data.inc.php';
$_KAT['mentors']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['mentors']       = '/tmp/inside';
$_KAT['mentors']['NOSEO']          = true;
$_KAT['onpage_def']['mentors']     = 20;
$_KAT['mentors']['acc_history']    = true;
$_KAT['INDEX']['mentors']          = 'id';

$_KAT['TABLES']['teachers']         = 'teachers';
$_KAT['SRC']['teachers']            = 'teachers';
$_KAT['TITLES']['teachers']         = 'Преподаватели';
$_KAT['FILES']['teachers']          = 'teachers.data.inc.php';
$_KAT['teachers']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['teachers']       = '/tmp/inside';
$_KAT['teachers']['NOSEO']          = true;
$_KAT['onpage_def']['teachers']     = 20;
$_KAT['teachers']['acc_history']    = true;
$_KAT['INDEX']['teachers']          = 'id';
/**** MENTORS **************/
$_KAT['mentors']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END MENTORS **********/

/**** EXPERTS **************/
$_KAT['experts']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END EXPERTS **********/

/**** TEACHERS **************/
$_KAT['teachers']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END TEACHERS **********/


/**** MANUALS **********/
$_KAT['onpage_def']['manuals'] = 50;
$_KAT['manuals']['adm_fields'] = array('name','anons','status_project','nomination_id','hidden');

// по-умолчанию на все запрет
$_KAT['manuals']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END MANUALS **********/

/**** EVENTS **********/
$_KAT['TABLES']['events']       = 'events';
$_KAT['SRC']['events']          = 'events';
$_KAT['TITLES']['events']       = 'Анонс лекций';
$_KAT['FILES']['events']        = 'org/events.data.inc.php';
$_KAT['events']['hidden']       = 'true'; // для db/list
$_KAT['DOC_PATH']['events']     = '/tmp/inside';
$_KAT['events']['NOSEO']        = true;
$_KAT['INDEX']['events']        = 'id';
$_KAT['onpage_def']['events']   = 20;
$_KAT['events']['adm_fields'] = array('name','event_date', 'event_time','hidden');
$_KAT['events']['acc_history']  = true;

$_KAT['events']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END EVENTS **********/

/**** FAQ **********/
$_KAT['onpage_def']['faq'] = 50;

// по-умолчанию на все запрет
$_KAT['faq']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END FAQ **********/

/**** FAQ **********/
$_KAT['onpage_def']['partners'] = 50;

// по-умолчанию на все запрет
$_KAT['partners']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END FAQ **********/

/**** TRANSLATIONS **********/
$_KAT['TABLES']['translations']     = 'translations';
$_KAT['SRC']['translations']        = 'translations';
$_KAT['TITLES']['translations']     = 'Трансляции';
$_KAT['FILES']['translations']      = 'org/translations.data.inc.php';
$_KAT['translations']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['translations']   = '/tmp/inside';
$_KAT['translations']['NOSEO']      = true;
$_KAT['onpage_def']['translations'] = 20;
$_KAT['translations']['acc_history'] = true;
$_KAT['INDEX']['translations']      = 'alias';
$_KAT['translations']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/org/translations.afteradd.inc.php';
$_KAT['translations']['adm_fields'] = array('name','anons','online','ts','hidden');


$_KAT['translations']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END TRANSLATIONS **********/

/**** EVENTS_SUBSCRIBE **********/
$_KAT['onpage_def']['events_subscribe']	= 200;
$_KAT['TABLES']['events_subscribe']     = 'events_subscribe';
$_KAT['SRC']['events_subscribe']        = 'events_subscribe';
$_KAT['TITLES']['events_subscribe']     = 'Зарегистрированные на лекции';
$_KAT['FILES']['events_subscribe']      = 'org/events_subscribe.data.inc.php';
$_KAT['events_subscribe']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['events_subscribe']   = '/tmp/inside';
$_KAT['events_subscribe']['NOSEO']      = true;
$_KAT['events_subscribe']['adm_fields'] = array('name','surname','email','event_id');
$_KAT['events_subscribe']['admin_search'] = array('event_id','email');

// по-умолчанию на все запрет
$_KAT['events_subscribe']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '1', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '1', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);

/**** Доступы к каталогам *****/
$_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']] = array(
    'projects',
    'news',
    'mentors',
    'experts',
    'teachers',
    'manuals',
    'events',
    'faq',
    'translations',
    'events_subscribe',
    'partners',
    'notes'
);
