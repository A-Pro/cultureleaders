<?php global $_CORE,$_CONF;
/**
 * Конфигурации каталогов для участников проекта
 */
/**** ACCOUNTS **********/
$_KAT['TABLES']['accounts']     = 'auth_pers';
$_KAT['SRC']['accounts']        = 'accounts';
$_KAT['TITLES']['accounts']     = Main::get_lang_str('accounts', 'db');
$_KAT['FILES']['accounts']      = 'root/accounts.data.inc.php';
$_KAT['accounts']['hidden']     = 'true'; // для db/list
//$_KAT['DOC_PATH']['accounts']   = '/tmp/inside';
$_KAT['accounts']['NOSEO']      = true;
$_KAT['accounts']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/root/accounts.afteradd.inc.php';
$_KAT['accounts']['acc_history'] = true;
$_KAT['INDEX']['accounts']      = 'author_id';

$_KAT['accounts']['PERM']['ua']	= '1'; // dobavlenie
$_KAT['accounts']['PERM']['ue']	= '0'; // redaktirovanie
$_KAT['accounts']['PERM']['ud']	= '1'; // udalenie
$_KAT['accounts']['PERM']['ui']	= '1'; // import
$_KAT['accounts']['PERM']['uf']	= '0'; // pokaz formy
$_KAT['accounts']['PERM']['uem']	= '1'; // ochistka tablicy
/**** END ACCOUNTS **********/

/**** PROJECTS **********/
$_KAT['projects']['NOSEO']         = true;
$_KAT['onpage_def']['projects']    = 1;
$_KAT['TABLES']['projects']        = 'projects';
$_KAT['TITLES']['projects']        = 'Проекты';
$_KAT['projects']['AFTER_ADD']     = 'db/projects/'.$_SESSION['SESS_AUTH']['ALL']['code_project'];
$_KAT['FILES']['projects']         = 'root/projects.data.inc.php';
$_KAT['projects']['hidden']        = 'true'; // для db/list
//$_KAT['DOC_PATH']['projects']      = '/tmp/inside';
$_KAT['projects']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/root/projects.afteradd.inc.php';
$_KAT['INDEX']['projects']         = 'alias';
$_KAT['projects']['acc_history']   = true;
$_KAT['SRC']['projects']           = 'projects_root';

// по-умолчанию на все запрет
$_KAT['projects']['PERM']['ua']     = '1'; // dobavlenie
$_KAT['projects']['PERM']['ue']     = '1'; // redaktirovanie
$_KAT['projects']['PERM']['ud']     = '1'; // udalenie
$_KAT['projects']['PERM']['ui']     = '1'; // import
$_KAT['projects']['PERM']['uf']     = '1'; // pokaz formy
$_KAT['projects']['PERM']['uem']    = '1'; // ochistka tablicy
// права на совершение действий
if( empty($_SESSION['SESS_AUTH']['ALL']['code_project'])){
    $_KAT['projects']['PERM']['ua']	= '0'; // dobavlenie
    $_KAT['projects']['PERM']['uf']	= '0'; // pokaz formy
}else {
    if($_SESSION['SESS_AUTH']['ALL']['role_project'] == 99){ // для лидера проекта
        $_KAT['projects']['PERM']['ue']	= '0'; // redaktirovanie
        $_KAT['projects']['PERM']['uf']	= '0'; // pokaz formy
    }elseif($_SESSION['SESS_AUTH']['ALL']['role_project'] < 20){
        $_KAT['projects']['PERM']['ua']	= '0'; // dobavlenie разрешим
        $_KAT['projects']['PERM']['uf']	= '0'; // pokaz formy
    }
}
/**** END PROJECTS **********/


/**** DOCS_ROOT **********/
$_KAT['docs_root']['NOSEO'] = true;
$_KAT['onpage_def']['docs_root']	= 999;
$_KAT['TABLES']['docs_root']		= 'docs';
$_KAT['TITLES']['docs_root']		= 'Файлы проекта';
$_KAT['docs_root']['AFTER_ADD']	= 'db/docs_root/';
$_KAT['FILES']['docs_root']		= 'root/docs.data.inc.php';
$_KAT['docs_root']['hidden']		= 'true'; // для db/list
//$_KAT['DOC_PATH']['docs_root']		= '/tmp/inside';
$_KAT['docs_root']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/root/docs.afteradd.inc.php';
$_KAT['docs_root']['AFTER_DEL_INC'] = $_CORE->SiteModDir.'/root/docs.afteradd.inc.php';
$_KAT['INDEX']['docs_root'] = 'alias';
$_KAT['docs_root']['acc_history'] = true;

// по-умолчанию на все запрет
$_KAT['docs_root']['PERM']['ua']     = '1'; // dobavlenie
$_KAT['docs_root']['PERM']['ue']     = '1'; // redaktirovanie
$_KAT['docs_root']['PERM']['ud']     = '1'; // udalenie
$_KAT['docs_root']['PERM']['ui']     = '1'; // import
$_KAT['docs_root']['PERM']['uf']     = '1'; // pokaz formy
$_KAT['docs_root']['PERM']['uem']    = '1'; // ochistka tablicy
// права на совершение действий
if( !empty($_SESSION['SESS_AUTH']['ALL']['code_project'])){
    if($_SESSION['SESS_AUTH']['ALL']['role_project'] == 99){ // для лидера проекта
        $_KAT['docs_root']['PERM']['ua']        = '0';
        $_KAT['docs_root']['PERM']['ue']    = '0'; // redaktirovanie
        $_KAT['docs_root']['PERM']['ud']        = '0'; // udalenie
        $_KAT['docs_root']['PERM']['uf']    = '0'; // pokaz formy
    }
}
/**** END DOCS_ROOT **********/

/**** PROJECTS_NUM **********/
$_KAT['projects_num']['NOSEO']         = true;
$_KAT['onpage_def']['projects_num']    = 99;
$_KAT['TABLES']['projects_num']        = 'projects';
$_KAT['TITLES']['projects_num']        = 'Проекты в номинации';
$_KAT['FILES']['projects_num']         = 'root/projects_num.data.inc.php';
$_KAT['projects_num']['hidden']        = 'true'; // для db/list
//$_KAT['DOC_PATH']['projects_num']      = '/tmp/inside';
$_KAT['INDEX']['projects_num']         = 'alias';
$_KAT['projects_num']['acc_history']   = true;
$_KAT['SRC']['projects_num']           = 'projects_num';
/**** END PROJECTS_NUM **********/

/**** MANUALS **********/
$_KAT['TABLES']['manuals']	= 'manuals';
$_KAT['SRC']['manuals']		= 'manuals';
$_KAT['TITLES']['manuals']	= 'Методические материалы';
$_KAT['FILES']['manuals']	    = 'root/manuals.data.inc.php';
$_KAT['manuals']['hidden']	= 'true'; // для db/list
//$_KAT['DOC_PATH']['manuals']	= '/tmp/inside';
$_KAT['manuals']['NOSEO']     = true;
$_KAT['onpage_def']['manuals'] = 99;
$_KAT['INDEX']['manuals']      = 'alias';
$_KAT['manuals']['acc_history'] = true;
/**** END MANUALS **********/

/**** LECTURES **********/
$_KAT['TABLES']['lectures']     = 'lectures';
$_KAT['SRC']['lectures']        = 'lectures';
$_KAT['TITLES']['lectures']     = 'Записи лекций';
$_KAT['FILES']['lectures']      = 'org/lectures.data.inc.php';
$_KAT['lectures']['hidden']     = 'true'; // для db/list
//$_KAT['DOC_PATH']['lectures']   = '/tmp/inside';
$_KAT['lectures']['NOSEO']      = true;
$_KAT['onpage_def']['lectures'] = 99;
$_KAT['lectures']['acc_history'] = true;
$_KAT['INDEX']['lectures']      = 'alias';
/**** END LECTURES **********/

$_KAT['TABLES']['translations']	= 'translations';
$_KAT['SRC']['translations']    = 'translations';
$_KAT['TITLES']['translations']	= 'Записи открытых лекций';
$_KAT['FILES']['translations']	= 'root/translations.data.inc.php';
$_KAT['translations']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['translations']	= '/tmp/inside';
$_KAT['translations']['NOSEO']     = true;
$_KAT['INDEX']['translations']     = 'alias';
$_KAT['onpage_def']['translations'] = 1;
$_KAT['translations']['NO_PLINE'] = true;
$_KAT['translations']['page_one'] = true;

/**** EVENTS **********/
$_KAT['TABLES']['events']       = 'events';
$_KAT['SRC']['events']          = 'events';
$_KAT['TITLES']['events']       = 'Лекций';
$_KAT['FILES']['events']        = 'root/events.data.inc.php';
$_KAT['events']['hidden']       = 'true'; // для db/list
$_KAT['DOC_PATH']['events']     = '/tmp/inside';
$_KAT['events']['NOSEO']        = true;
$_KAT['INDEX']['events']        = 'alias';
$_KAT['onpage_def']['events']   = 100;
/**** END EVENTS **********/

/***** SETTINGS FOR DATE STAGE *****/

if($_CONF['settings']['stage1_off'] > '0000-00-00' && date('Y-m-d')> $_CONF['settings']['stage1_off']){
    $_KAT['projects']['PERM']['ua'] = '1'; // dobavlenie
    $_KAT['projects']['PERM']['uf'] = '1'; // pokaz formy
    $_KAT['projects']['PERM']['ue'] = '1'; // redaktirovanie

    $_KAT['docs_root']['PERM']['ua']    = '1'; // dobavlenie
    $_KAT['docs_root']['PERM']['ue']    = '1'; // redaktirovanie
    $_KAT['docs_root']['PERM']['ud']    = '1'; // udalenie
    $_KAT['docs_root']['PERM']['uf']    = '1'; // pokaz formy
}
// изменение прав в зависимости от стадий (наступления ключевый дат) конкурса,
// определены в конфигураторе дат в ЛК Организитора
// для лидера проекта
if( !empty($_SESSION['SESS_AUTH']['ALL']['code_project']) && $_SESSION['SESS_AUTH']['ALL']['role_project'] == 99) {

    if ($_CONF['settings']['stage2_on'] > '0000-00-00' && date('Y-m-d') >= $_CONF['settings']['stage2_on']) {
        $_KAT['projects']['PERM']['uf'] = '0'; // pokaz formy
        $_KAT['projects']['PERM']['ue'] = '0'; // redaktirovanie

        $_KAT['docs_root']['PERM']['ua']    = '0'; // dobavlenie
        $_KAT['docs_root']['PERM']['ue']    = '0'; // redaktirovanie
        $_KAT['docs_root']['PERM']['ud']    = '0'; // udalenie
        $_KAT['docs_root']['PERM']['uf']    = '0'; // pokaz formy
    }
    if ($_CONF['settings']['stage2_off'] > '0000-00-00' && date('Y-m-d') > $_CONF['settings']['stage2_off']) {
        $_KAT['projects']['PERM']['ua'] = '1'; // dobavlenie
        $_KAT['projects']['PERM']['uf'] = '1'; // pokaz formy
        $_KAT['projects']['PERM']['ue'] = '1'; // redaktirovanie

        $_KAT['docs_root']['PERM']['ua'] = '1'; // dobavlenie
        $_KAT['docs_root']['PERM']['ue'] = '1'; // redaktirovanie
        $_KAT['docs_root']['PERM']['ud'] = '1'; // udalenie
        $_KAT['docs_root']['PERM']['uf'] = '1'; // pokaz formy
    }
    /* $_CONF['settings']['stage3_on'] > '0000-00-00' && date('Y-m-d') >= $_CONF['settings']['stage3_on']*/
    if (date('Y-m-d') >= '2019-11-25' ) {
        $_KAT['projects']['PERM']['uf'] = '0'; // pokaz formy
        $_KAT['projects']['PERM']['ue'] = '0'; // redaktirovanie

        $_KAT['docs_root']['PERM']['ua']    = '0'; // dobavlenie
        $_KAT['docs_root']['PERM']['ue']    = '0'; // redaktirovanie
        $_KAT['docs_root']['PERM']['ud']    = '0'; // udalenie
        $_KAT['docs_root']['PERM']['uf']    = '0'; // pokaz formy

    }
    /*if ($_CONF['settings']['stage3_off'] > '0000-00-00' && date('Y-m-d') > $_CONF['settings']['stage3_off']) {*/
    if (date('Y-m-d') > '2019-12-10') {
        $_KAT['projects']['PERM']['ua'] = '1'; // dobavlenie
        $_KAT['projects']['PERM']['uf'] = '1'; // pokaz formy
        $_KAT['projects']['PERM']['ue'] = '1'; // redaktirovanie

        $_KAT['docs_root']['PERM']['ua'] = '1'; // dobavlenie
        $_KAT['docs_root']['PERM']['ue'] = '1'; // redaktirovanie
        $_KAT['docs_root']['PERM']['ud'] = '1'; // udalenie
        $_KAT['docs_root']['PERM']['uf'] = '1'; // pokaz formy
    }
    if ($_CONF['settings']['final'] > '0000-00-00' && date('Y-m-d') >= $_CONF['settings']['final']) {

    }
}