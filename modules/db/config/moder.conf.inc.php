<?php
unset($_KAT['ADM_MENU']);
$_KAT['ADM_MENU']["/db/projects/"] = array("/ico/a_tree.gif", "Список проектов", "db/projects/");


/**** PROJECTS **********/
$_KAT['projects']['NOSEO']      = true;
$_KAT['onpage_def']['projects']	= 50;
$_KAT['TABLES']['projects']		= 'projects';
$_KAT['TITLES']['projects']		= 'Список проектов';
$_KAT['projects']['AFTER_ADD']	= 'db/projects/';
$_KAT['FILES']['projects']		= 'moder/projects.data.inc.php';
$_KAT['projects']['hidden']		= 'true'; // для db/list
$_KAT['DOC_PATH']['projects']		= '/tmp/inside';
//$_KAT['projects']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/moder/projects.afteradd.inc.php';
$_KAT['INDEX']['projects'] = 'alias';
$_KAT['projects']['acc_history'] = true;
$_KAT['projects']['adm_fields'] = array('name','anons','complit','status');
$_KAT['projects']['admin_search'] = array('name','nomination_id','status','complit');

// по-умолчанию на все запрет
$_KAT['projects']['PERM'] = array(
'ua'    => '1', // dobavlenie
'ue'    => '0', // redaktirovanie
'ud'    => '1', // udalenie
'ui'    => '1', // import
'uf'    => '0', // pokaz formy
'uem'   => '1', // ochistka tablicy
);

/**** END PROJECTS **********/


/**** Доступы к каталогам *****/
$_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']] = array(
    'projects'
);