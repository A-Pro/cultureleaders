<?php

/**** ACCOUNTS **********/
$_KAT['onpage_def']['accounts']	= 50;
$_KAT['TABLES']['accounts']     = 'auth_pers';
$_KAT['SRC']['accounts']        = 'accounts';
$_KAT['TITLES']['accounts']     = 'Список участников';
$_KAT['FILES']['accounts']      = 'mentor/accounts.data.inc.php';
$_KAT['accounts']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['accounts']   = '/tmp/inside';
$_KAT['accounts']['NOSEO']      = true;
$_KAT['INDEX']['accounts']      = 'author_id';
$_KAT['accounts']['acc_history'] = true;
$_KAT['accounts']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/mentor/accounts.afteradd.inc.php';

// по-умолчанию на все запрет
$_KAT['accounts']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END ACCOUNTS **********/


/**** PROJECTS **********/
$_KAT['projects']['NOSEO']         = true;
$_KAT['onpage_def']['projects']    = 999;
$_KAT['TABLES']['projects']        = 'projects';
$_KAT['TITLES']['projects']        = 'Проекты';
$_KAT['projects']['AFTER_ADD']     = 'db/projects/';
$_KAT['FILES']['projects']         = 'mentor/projects.data.inc.php';
$_KAT['projects']['hidden']        = 'true'; // для db/list
$_KAT['DOC_PATH']['projects']      = '/tmp/inside';
$_KAT['projects']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/root/projects.afteradd.inc.php';
$_KAT['INDEX']['projects']         = 'alias';
$_KAT['projects']['acc_history']   = true;
$_KAT['SRC']['projects']           = 'projects_mentor';

/**** END PROJECTS **********/


/**** DOCS **********/
$_KAT['docs']['NOSEO']          = true;
$_KAT['onpage_def']['docs']     = 999;
$_KAT['TABLES']['docs']         = 'docs';
$_KAT['TITLES']['docs']         = 'Файлы проекта';
$_KAT['docs']['AFTER_ADD']      = 'db/docs/';
$_KAT['FILES']['docs']          = 'root/docs.data.inc.php';
$_KAT['docs']['hidden']         = 'true'; // для db/list
$_KAT['DOC_PATH']['docs']       = '/tmp/inside';
$_KAT['INDEX']['docs']          = 'alias';
$_KAT['docs']['acc_history']    = true;
$_KAT['SRC']['docs']            = 'docs_root';
/**** END DOCS **********/

/**** NOMINATIONS **********/
$_KAT['onpage_def']['nominations']  = 1;
$_KAT['TABLES']['nominations']     = 'nominations';
$_KAT['SRC']['nominations']        = 'nominations';
$_KAT['TITLES']['nominations']     = 'Список участников';
$_KAT['nominations']['AFTER_ADD']  = 'db/nominations/';
$_KAT['FILES']['nominations']      = 'mentor/nominations.data.inc.php';
$_KAT['nominations']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['nominations']   = '/tmp/inside';
$_KAT['nominations']['NOSEO']      = true;
$_KAT['INDEX']['nominations']      = 'alias';
$_KAT['nominations']['acc_history'] = true;
$_KAT['SRC']['nominations']        = 'nominations_mentor';
$_KAT['INDEX']['nominations']      = 'alias';
$_KAT['nominations']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/mentor/nominations.afteradd.inc.php';

$_KAT['nominations']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END NOMINATIONS **********/

/**** MANUALS **********/
$_KAT['FILES']['manuals']         = 'mentor/manuals.data.inc.php';
$_KAT['manuals']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/mentor/manuals.afteradd.inc.php';
$_KAT['manuals']['AFTER_DEL_INC'] = $_CORE->SiteModDir.'/mentor/manuals.afteradd.inc.php';

$_KAT['manuals']['PERM'] = array(
    'ua'    => '0', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '0', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END MANUALS **********/