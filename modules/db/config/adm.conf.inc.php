<?php
// интерфейс админа будет расширять возможности организатора
include 'org.conf.inc.php';
$_KAT['ADM_MENU']["/db/news/"] = array("/ico/a_tree.gif", "Новости", "db/news/");
$_KAT['ADM_MENU']["/db/mentors/"] = array("/ico/a_tree.gif", "Менторы", "db/mentors/");
$_KAT['ADM_MENU']["/db/experts/"] = array("/ico/a_tree.gif", "Эксперты", "db/experts/");
$_KAT['ADM_MENU']["/db/teachers/"] = array("/ico/a_tree.gif", "Преподаватели", "db/teachers/");
$_KAT['ADM_MENU']["/db/docs/"] = array("/ico/a_tree.gif", "Документы для проектов", "db/docs/");
$_KAT['ADM_MENU']["/db/regions/"] = array("/ico/a_tree.gif", "Регионы", "db/regions/");
$_KAT['ADM_MENU']["/db/faq/"] = array("/ico/a_tree.gif", "Вопросы", "db/faq/");


/**** USERS_ROLE **********/
$_KAT['onpage_def']['users_role']	= 50;
$_KAT['TABLES']['users_role']     = 'auth_pers';
$_KAT['SRC']['users_role']        = 'users_mentors';
$_KAT['TITLES']['users_role']     = 'Роли пользователей';
$_KAT['FILES']['users_role']      = 'adm/users_role.data.inc.php';
$_KAT['users_role']['hidden']     = 'true'; // для db/list
$_KAT['DOC_PATH']['users_role']   = '/tmp/inside';
$_KAT['users_role']['NOSEO']      = true;
$_KAT['INDEX']['users_role']      = 'author_id';
$_KAT['users_role']['adm_fields'] = array('author_comment','author_login','account_type','nomination_id');
$_KAT['users_role']['admin_search'] = array('author_comment','author_login','account_type','nomination_id');
$_KAT['users_role']['acc_history'] = true;
$_KAT['users_role']['AFTER_ADD_INC'] = $_CORE->SiteModDir.'/org/accounts.afteradd.inc.php';

// по-умолчанию на все запрет
$_KAT['users_role']['PERM'] = array(
    'ua'    => '1', // dobavlenie
    'ue'    => '0', // redaktirovanie
    'ud'    => '1', // udalenie
    'ui'    => '1', // import
    'uf'    => '0', // pokaz formy
    'uem'   => '1', // ochistka tablicy
);
/**** END USERS_ROLE **********/

$_KAT['TABLES']['docs']	    = 'docs';
$_KAT['SRC']['docs']		= 'docs';
$_KAT['TITLES']['docs']	    = 'Документы для проектов';
$_KAT['FILES']['docs']	    = 'docs.data.inc.php';
$_KAT['docs']['hidden']	= 'true'; // для db/list
$_KAT['DOC_PATH']['docs']	= '/tmp/inside';
$_KAT['docs']['NOSEO'] = true;
$_KAT['docs']['acc_history'] = true;
$_KAT['INDEX']['docs']          = 'alias';
//$_KAT['docs']['adm_fields'] = array('author_login','author_comment','account_type','ts','hidden');

/**** Доступы к каталогам *****/
$add_perm = array(
    'settings','news','mentors','experts','teachers','events','docs','regions','faq'
);
$_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']] =
    array_merge($_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']], $add_perm);