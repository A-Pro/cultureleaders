<?php

$FORM_WHERE = '';
if (empty($_SESSION['SESS_AUTH']['ID'])) {
    header("Location: /auth/login");
    exit;
}

if (!$_CORE->IS_ADMIN) {
    $FORM_ORDER = ' ';
} else {
    $FORM_ORDER = '  ';
}
if (!$_CORE->IS_ADMIN) {
    $FORM_WHERE = "AND (hidden != 1 OR hidden IS NULL) ";
} else {
    $FORM_WHERE = "";
}


$FORM_DATA = array(
    'id' =>
        array(
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array(
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 1,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'ts' =>
        array(
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default' => date('Y-m-d')
        ),
    'stage1_off' =>
        array(
            'field_name' => 'stage1_off',
            'name' => 'form[stage1_off]',
            'title' => 'Дата завершения 1-го этапа',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage2_on' =>
        array(
            'field_name' => 'stage2_on',
            'name' => 'form[stage2_on]',
            'title' => 'Завершение отбора достойных проектов',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage2_off' =>
        array(
            'field_name' => 'stage2_off',
            'name' => 'form[stage2_off]',
            'title' => 'Дата завершения 2-го этапа',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage3_on' =>
        array(
            'field_name' => 'stage3_on',
            'name' => 'form[stage3_on]',
            'title' => 'Завершение выбора номинантов',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
    'stage3_off' =>
        array(
            'field_name' => 'stage3_off',
            'name' => 'form[stage3_off]',
            'title' => 'Дата завершения 3-го этапа',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),

    'final' =>
        array(
            'field_name' => 'final',
            'name' => 'form[final]',
            'title' => 'Подведение итогов и определение победителей',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'placeholder' => date('Y-m-d')
        ),
);
