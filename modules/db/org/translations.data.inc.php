<?php
global $_CORE, $_CONF,$FORM_FIELD4ALIAS;
$FORM_FIELD4ALIAS = 'alias';
$FORM_ORDER	= ' ORDER BY online DESC, ts DESC ';

$FORM_WHERE	= "";

$FORM_DATA= array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'online'	=>
        array (
            'field_name' => 'online',
            'name' => 'form[online]',
            'title' => 'Сейчас онлайн',
            'must' => 0,
            'maxlen' => 1,
            'type' => 'select',
            'arr' => array(0=>'', 1=>'<h2 class="text-lg-center"><i class="fa fa-bullhorn"></i></h2>'),
        ),
    'name'	=>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => Main::get_lang_str('name', 'db'),
            'must' => 1,
            'style'	=> 'width:100%',
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'code'	=>
        array (
            'field_name' => 'code',
            'name' => 'form[code]',
            'title' => 'Ссылка на YouTube ролик',
            'must' => 1,
            'style'	=> 'width:100%',
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Краткое описание (для списка)',
        'must' => '0',
        'maxlen' => '600',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '10',
    ),
    'ts'	=>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => 'Дата трансляции',
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'default'	=> date('Y-m-d')
        ),
    'hidden' =>
        array (
            'field_name' => 'hidden',
            'name' => 'form[hidden]',
            'title' => Main::get_lang_str('ne_publ', 'db'),
            'must' => 0,
            'maxlen' => 1,
            'type' => 'checkbox'
        ),
);

if(isset($_GET['edit'])){
    $FORM_DATA['online']['type'] = 'checkbox';
    $FORM_DATA['online']['sub_type'] = 'int';
    $FORM_DATA['online']['default'] = '0';
}