<?php
global $_CORE, $_CONF, $FORM_FIELD4ALIAS;
$FORM_WHERE ='';
$FORM_ORDER = ' ORDER BY hidden ASC, event_date ASC , event_time ASC';
$FORM_FIELD4ALIAS = 'alias';
$FORM_DATA = array (
    'id' =>
        array (
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array (
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => Main::get_lang_str('alias', 'db'),
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
            'default' => uniqid(),
        ),
    'name' =>
        array (
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Тема',
            'must' => 1,
            'style'	=> 'width:100%',
            'type' => 'textbox',
            'logic' => 'OR',
            'search' => " LIKE '%%%s%%'",
        ),
    'teacher_id' => array(
        'field_name' => 'teacher_id',
        'name' => 'form[teacher_id]',
        'title' => 'Спикер',
        'must' => '1',
        'type' => 'multiselect_from_table',
        'ex_table' => DB_TABLE_PREFIX.'teachers',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'maxlen' => '128',
        'prompt' => '',
    ),

    'event_date' =>
        array (
            'field_name' => 'event_date',
            'name' => 'form[event_date]',
            'title' => 'Дата проведения',
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'datetime',
            'default'	=> date('Y-m-d'),
            'dateformat' => 'Y-m-d',
            'timepicker' => 'false',
            'sub_type' => 'varchar'
        ),

    'event_time' =>
        array (
            'field_name' => 'event_time',
            'name' => 'form[event_time]',
            'title' => 'Время проведения',
            'must' => 1,
            'maxlen' => 255,
            'placeholder' => '00:00 - 23:59',
            'type' => 'textbox',
            'default'	=> '19:30 - 21:00',
        ),

    'news_id' => array(
        'field_name' => 'news_id',
        'name' => 'form[news_id]',
        'title' => 'Новостной анонс события',
        'must' => '0',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'news',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'maxlen' => '128',
        'prompt' => '',
        'example' => 'Выберите новость, чтобы отобразился баннер "Главное событие скоро" на главной странице',
    ),

    'ts' =>
        array (
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => Main::get_lang_str('data', 'db'),
            'must' => 1,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'default'	=> date('Y-m-d H:i:s'),
        ),
    'hidden' =>
        array (
            'field_name' => 'hidden',
            'name' => 'form[hidden]',
            'title' => Main::get_lang_str('ne_publ', 'db'),
            'must' => 0,
            'maxlen' => 1,
            'type' => 'checkbox',
            'sub_type' => 'int'
        ),
);