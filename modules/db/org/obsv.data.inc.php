<?php
$FORM_ORDER = ' ORDER BY id DESC ';

$FORM_WHERE = '';
//$FORM_WHERE = ' AND (hidden IS NULL OR hidden = 0)';

$FORM_DATA = array(
    'id' =>
        array(
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array(
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => 'alias',
            'must' => 0,
            'maxlen' => 255,
            'type' => 'hidden',
        ),
    'name' =>
        array(
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Дата отправки вопроса',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'default' => date('Y-m-d H:i:s')
        ),
    'person' =>
        array(
            'field_name' => 'person',
            'name' => 'form[person]',
            'title' => 'ФИО',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'email' =>
        array(
            'field_name' => 'email',
            'name' => 'form[email]',
            'title' => 'E-mail',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'phone' =>
        array(
            'field_name' => 'phone',
            'name' => 'form[phone]',
            'title' => 'Телефон',
            'must' => 0,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'ts' =>
        array(
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => 'Дата',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default' => date('Y-m-d')
        ),
    'cont' =>
        array(
            'field_name' => 'cont',
            'name' => 'form[cont]',
            'title' => 'Вопрос',
            'must' => 0,
            'maxlen' => '65535',
            'type' => 'textarea',
            'style' => 'width:100%',
            'rows' => '20',
        ),
    'hidden'	=> array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => Main::get_lang_str('ne_publ', 'db'),
        'must' => 0,
        'maxlen' => 1,
        'type' => 'checkbox',
    ),
);
