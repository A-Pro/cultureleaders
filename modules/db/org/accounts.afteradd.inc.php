<?php
$sql = new SQL();
$data = $_KAT[$_KAT['KUR_ALIAS']]['last_form'];
$code_project = $data['code_project'];
// пропишем для создавшего код проекта статус лидера проекта
if(!empty($code_project) && $data['role_project'] == '99'){
    $sql->upd(
        DB_TABLE_PREFIX.'auth_pers',
        " `role_project` = '20' ",
        "author_id != '".$data['author_id']."' AND `code_project` = '{$code_project}' AND `role_project` = '99'"
    );
}
if(in_array($data['account_type'], array(ACCTYPE_ORG,ACCTYPE_MODER,ACCTYPE_EDIT,ACCTYPE_ADM))){
    $author_role = $sql->getval('author_role',DB_TABLE_PREFIX.'auth_pers',"author_id = '".$data['author_id']."'");
    if($author_role!='admin'){
        $sql->upd(
            DB_TABLE_PREFIX.'auth_pers',
            " `author_role` = 'admin' ",
            "author_id = '".$data['author_id']."'"
        );
    }
}