<?php
global $_CORE, $_ACCESS, $_CONF, $FORM_FIELD4ALIAS;

if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }

$FORM_FIELD4ALIAS = 'alias';

$FORM_WHERE = " AND ".$_KAT['KUR_TABLE'].".`account_type` = '".ACCTYPE_ROOT."'";

//$FORM_WHERE	.= " AND ".$_KAT['KUR_TABLE'].".`role_project` > '10'";

$FORM_ORDER	= ' ORDER BY '.$_KAT['KUR_TABLE'].'.`role_project` DESC';

//unset($_CONF['role_project'][0],$_CONF['role_project']['10']);
$_CONF['role_project'][0]='';
$FORM_DATA= array (
    'author_id' => array (
        'field_name' => 'author_id',
        'name' => 'form[author_id]',
        'title' => '',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default'   => time(),
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'role_project' => array (
        'field_name' => 'role_project',
        'name' => 'form[role_project]',
        'title' => 'Роль в проекте',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'select',
        "arr"   => $_CONF['role_project'],
    ),
    'author_login' => array (
        'field_name' => 'author_login',
        'name' => 'form[author_login]',
        'title' => 'E-mail',
        'must' => 0,
        'mask_email' => true,
        'type' => 'textbox',
        'search'    => " LIKE '%%%s%%' ",
    ),
    'author_comment' => array (
        'field_name' => 'author_comment',
        'name' => 'form[author_comment]',
        'title' => 'Имя',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'search'    => " LIKE '%%%s%%' ",
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'author_surname' => array (
        'field_name' => 'author_surname',
        'name' => 'form[author_surname]',
        'title' => 'Фамилия',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'author_patronymic' => array (
        'field_name' => 'author_patronymic',
        'name' => 'form[author_patronymic]',
        'title' => 'Отчество',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'author_phone' => array (
        'field_name' => 'author_phone',
        'name' => 'form[author_phone]',
        'title' => 'Контактный телефон',
        'must' => 0,
        'size' => 24,
        'maxlen' => 24,
        'type' => 'textbox',
        'placeholder'   => '+7 ___ ___ __ __',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),
    'gender' =>
        array (
            'field_name' => 'gender',
            'name' => 'form[gender]',
            'title' => 'Пол',
            'type' => 'select',
            'must' => '0',
            'arr' => array( ''=>'...','муж'=>'муж','жен'=>'жен'),
            'maxlen' => '3',
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',
        ),
    'author_age' =>
        array (
            'field_name' => 'author_age',
            'name' => 'form[author_age]',
            'title' => 'Возраст',
            'type' => 'select',
            'must' => '0',
            'arr' => array( ''=>'...','18 – 24'=>'18 – 24', '25-34'=>'25-34', '35-45'=>'35-45', '45 и выше'=>'45 и выше'),
            'maxlen' => '24',
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',
        ),
    'region_id' =>
        array (
            'field_name' => 'region_id',
            'name' => 'form[region_id]',
            'title' => 'Регион проживания',
            'must' => '0',
            'maxlen' => '255',
            'type' => 'select_from_table',
            'sub_type' => 'int',
            'ex_table' => DB_TABLE_PREFIX.'regions',
            'id_ex_table' => 'id',
            'ex_table_field' => 'name',
            // 'arr'	=> $_CONF['REGIONS'],
            'style' => 'width:100%',
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',
        ),
    'city' => array (
        'field_name' => 'city',
        'name' => 'form[city]',
        'title' => 'Город\ нас. пункт',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),
    'education' =>
        array (
            'field_name' => 'education',
            'name' => 'form[education]',
            'title' => 'Моё образование',
            'type' => 'select',
            'sub_type' => 'int',
            'must' => '0',
            'arr' => array(
                0 => '...',
                1 => 'Неполное среднее',
                2 => 'Среднее/ среднее-специальное',
                3 => 'Неполное высшее',
                4 => 'Высшее',
                5 => 'Несколько высших',
                6 => 'Есть ученая степень',
            ),
            'maxlen' => '1',
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',
        ),
    'achievement' => array (
        'field_name' => 'achievement',
        'name' => 'form[achievement]',
        'title' => 'Профессиональные достижения',
        'must' => '0',
        'maxlen' => '600',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '5',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),
    'realized_projects' => array (
        'field_name' => 'realized_projects',
        'name' => 'form[realized_projects]',
        'title' => 'Реализованные проекты в сфере культуры и креативных индустрий',
        'must' => '0',
        'maxlen' => '600',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '5',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),
    'motivation_letter' => array (
        'field_name' => 'motivation_letter',
        'name' => 'form[motivation_letter]',
        'title' => 'Мотивационное письмо <span class="t-text t-text_xs">цель участия в программе, мои ожидания от программы, как мой проект изменит город, не более 500 знаков без пробелов</span>',
        'must' => '0',
        'maxlen' => '950',
        'type' => 'textarea',
        'style' => 'width:100%',
        'cols' => '50',
        'rows' => '10',
        'placeholder'=>'до 500 знаков без пробелов',
        'class' => 't-input',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE,false)',
    ),

    'code_project' =>
        array (
            'field_name' => 'code_project',
            'name' => 'form[code_project]',
            'title' => 'Проект',
            'must' => '0',
            'maxlen' => '255',
            'type' => 'select_from_table',
            'ex_table' => DB_TABLE_PREFIX.'projects',
            'id_ex_table' => 'alias',
            'ex_table_field' => 'name',
            // 'arr'	=> $_CONF['REGIONS'],
            'style' => 'width:100%',
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',

        ),
    'complit' => array(
        'field_name' => 'complit',
        'name' => 'form[complit]',
        'title' => 'Профиль, %',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
    )
);
if(isset($_GET['edit'])){
    $FORM_DATA['code_project']['type'] = 'hidden';
    unset($FORM_DATA['author_login'],$FORM_DATA['complit']);
}

if( in_array($_SESSION['SESS_AUTH']['LOGIN'], array('anna.terkina@rsv.ru','organizator','developer') ) ){
    unset($FORM_DATA['author_login']['mask_email']);
}
