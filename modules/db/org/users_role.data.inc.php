<?php
global $_CORE, $FORM_FIELD4ALIAS, $_ACCESS, $_CONF;

if (empty($_SESSION['SESS_AUTH']['ID'] )) { header ("Location: /auth/login"); exit; }


$FORM_WHERE = " AND ".$_KAT['KUR_TABLE'].".`account_type` != '".ACCTYPE_ADM."'";

$FORM_WHERE	.= " AND (".$_KAT['KUR_TABLE'].".`code_project` = '' OR ".$_KAT['KUR_TABLE'].".`code_project` IS NULL)";

$FORM_ORDER	= ' ORDER BY '.$_KAT['KUR_TABLE'].'.`account_type` DESC ';
$FORM_FIELD4ALIAS = 'alias';

$FORM_DATA= array (
    'author_id' => array (
        'field_name' => 'author_id',
        'name' => 'form[author_id]',
        'title' => '',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
        'default'   => time(),
    ),
    'alias' => array (
        'field_name' => 'alias',
        'name' => 'form[alias]',
        'title' => 'alias',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
    'author_login' => array (
        'field_name' => 'author_login',
        'name' => 'form[author_login]',
        'title' => 'E-mail',
        'must' => 0,
        'mask_email' => true,
        'type' => 'textbox',
        'search'    => " LIKE '%%%s%%' ",
    ),
    'author_comment' => array (
        'field_name' => 'author_comment',
        'name' => 'form[author_comment]',
        'title' => 'Имя',
        'must' => 1,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'search'    => " LIKE '%%%s%%' ",
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'author_surname' => array (
        'field_name' => 'author_surname',
        'name' => 'form[author_surname]',
        'title' => 'Фамилия',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'author_patronymic' => array (
        'field_name' => 'author_patronymic',
        'name' => 'form[author_patronymic]',
        'title' => 'Отчество',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'textbox',
        'placeholder'   => '',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'make_value_func' => 'htmlspecialchars($value, ENT_QUOTES, CODEPAGE)',
    ),
    'region_id' =>
        array (
            'field_name' => 'region_id',
            'name' => 'form[region_id]',
            'title' => 'Регион проживания',
            'must' => '0',
            'maxlen' => '255',
            'type' => 'select_from_table',
            'sub_type' => 'int',
            'ex_table' => DB_TABLE_PREFIX.'regions',
            'id_ex_table' => 'id',
            'ex_table_field' => 'name',
            // 'arr'	=> $_CONF['REGIONS'],
            'style' => 'width:100%',
            'class'    => ' t-input ',
            'tr_class' => 'class="t431__oddrow"',
        ),

    'account_type' => array (
        'field_name' => 'account_type',
        'name' => 'form[account_type]',
        'title' => 'Роль',
        'must' => 0,
        'size' => 50,
        'maxlen' => 255,
        'type' => 'select',
        "arr"   => array(
            ACCTYPE_ROOT    => $_CONF['account_type'][ACCTYPE_ROOT],
            ACCTYPE_MENTOR  => $_CONF['account_type'][ACCTYPE_MENTOR],
            ACCTYPE_ORG     => $_CONF['account_type'][ACCTYPE_ORG],
            ACCTYPE_MODER   => $_CONF['account_type'][ACCTYPE_MODER],
            ACCTYPE_EDIT    => $_CONF['account_type'][ACCTYPE_EDIT],
        ),
    ),
    'nomination_id' => array(
        'field_name' => 'nomination_id',
        'name' => 'form[nomination_id]',
        'title' => 'Номинация',
        'must' => '0',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'nominations',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'name',
        //'ex_table_where'  => " from_group = '". $_SESSION['SESS_AUTH']['ALL']['from_group']."' ",
        'also' => 'class=""',
        'maxlen' => '128',
        'show' => array ( 'account_type' => ACCTYPE_MENTOR )
    ),
);
if(isset($_GET['edit'])){
    unset($FORM_DATA['author_login']);
}
