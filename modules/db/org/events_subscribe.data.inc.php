<?php
$FORM_ORDER = ' ORDER BY event_id ASC ';

$FORM_WHERE = '';
//$FORM_WHERE = ' AND (hidden IS NULL OR hidden = 0)';

$FORM_DATA = array(
    'id' =>
        array(
            'field_name' => 'id',
            'name' => 'form[id]',
            'title' => 'id',
            'must' => 0,
            'maxlen' => 20,
            'type' => 'hidden',
        ),
    'alias' =>
        array(
            'field_name' => 'alias',
            'name' => 'form[alias]',
            'title' => 'alias',
            'must' => 0,
            'maxlen' => 255,
            'type' => 'hidden',
        ),
    'name' =>
        array(
            'field_name' => 'name',
            'name' => 'form[name]',
            'title' => 'Имя',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
            'default' => date('Y-m-d H:i:s')
        ),
    'surname' =>
        array(
            'field_name' => 'surname',
            'name' => 'form[surname]',
            'title' => 'Фамилия',
            'must' => 1,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'email' =>
        array(
            'field_name' => 'email',
            'name' => 'form[email]',
            'title' => 'E-mail',
            'must' => 0,
            'mask_email' => true,
            'style' => 'width:100%',
            'maxlen' => 255,
            'type' => 'textbox',
        ),
    'event_id' => array(
        'field_name' => 'event_id',
        'name' => 'form[event_id]',
        'title' => 'Лекция',
        'must' => '1',
        'type' => 'select_from_table',
        'ex_table' => DB_TABLE_PREFIX.'events',
        'id_ex_table' => 'alias',
        'ex_table_field' => 'CONCAT('.DB_TABLE_PREFIX.'events.event_date," ~ ",'.DB_TABLE_PREFIX.'events.name)',
        'class'    => ' t-input ',
        'tr_class' => 'class="t431__oddrow"',
        'maxlen' => '255',
    ),
    'ts' =>
        array(
            'field_name' => 'ts',
            'name' => 'form[ts]',
            'title' => 'Дата',
            'must' => 0,
            'size' => 15,
            'maxlen' => 255,
            'type' => 'hidden',
            'readonly' => 'true',
            'default' => date('Y-m-d')
        )
);
