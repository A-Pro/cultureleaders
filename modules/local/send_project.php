<?php
global $SQL_DBLINK;

// reg only и если есть проект и определена роль проекта как лидер
if (
    empty($_SESSION['SESS_AUTH']['ID']) ||
    empty($_SESSION['SESS_AUTH']['ALL']['code_project']) ||
    $_SESSION['SESS_AUTH']['ALL']['role_project'] != 99
) exit;
$sql = new SQL();
if(empty($SQL_DBLINK)) $sql->connect();

$code_project = $_SESSION['SESS_AUTH']['ALL']['code_project'];

$sql->upd(
    DB_TABLE_PREFIX.'projects',
    "`status` = 0 ",
    "alias = '".$code_project."'");

header("Location: /empty/db/projects/".$code_project);
exit;
