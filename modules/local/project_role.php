<?php
global $_ACCESS,$SQL_DBLINK;

// reg only
if (empty($_SESSION['SESS_AUTH']['ID'])) exit;
$sql = new SQL();
if(empty($SQL_DBLINK)) $sql->connect();
$user_id = intval($_GET['user_id']);
$new_role = '';
if(isset($_GET['no'])) $new_role = 10;
if(isset($_GET['yes'])) $new_role = 20;
if(isset($_GET['main'])) $new_role = 99;
if(isset($_GET['out'])) {$new_role = 0; $user_id = $_SESSION['SESS_AUTH']['ID']; }
if(($_SESSION['SESS_AUTH']['ALL']['role_project'] == 99 || isset($_GET['out'])) && !empty($_SESSION['SESS_AUTH']['ALL']['code_project'])) {
    if ($new_role !== '' && !empty($user_id)) {
        if($new_role == 0){
            $code_project = '';
            $_SESSION['SESS_AUTH']['ALL']['code_project'] = '';
            $_SESSION['SESS_AUTH']['ALL']['role_project'] = 0;
        }
        $code_project = $_SESSION['SESS_AUTH']['ALL']['code_project'];

        $sql->upd(DB_TABLE_PREFIX . 'auth_pers',
            "`code_project` = '{$code_project}', `role_project` = '{$new_role}' ",
            "author_id = '" . $user_id . "'");
        if ($new_role == 99) {
            $sql->upd(DB_TABLE_PREFIX . 'auth_pers',
                "`code_project` = '{$code_project}', `role_project` = '20'",
                "author_id = '" . $_SESSION['SESS_AUTH']['ID'] . "'");
        }
    }
}
if(!isset($_GET['out'])) {
    header("Location: /empty/db/accounts?ajax_data");
}
die('ok');