<?php
global $_CORE, $_CONF, $SQL_DBLINK;
if($_CORE->IS_ADMIN) {
    $oSQL = new SQL();
    $result = $oSQL->upd(
        DB_TABLE_PREFIX . 'events_subscribe as t1 '.
            'RIGHT JOIN '.DB_TABLE_PREFIX . 'auth_pers as t2 ON t1.email = t2.author_login',
        't1.user_id = t2.author_id','t1.id IS NOT NULL');
    debug($result);
}