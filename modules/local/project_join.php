<?php
global $SQL_DBLINK;

// reg only
if (empty($_SESSION['SESS_AUTH']['ID'])) exit;
$sql = new SQL();
if(empty($SQL_DBLINK)) $sql->connect();
$agree_normativ = intval($_POST['agree_normativ']);
$code_project = mysqli_real_escape_string($SQL_DBLINK,$_POST['code_project']);

if(!empty($code_project) && $agree_normativ == 1){
    $code_project = str_replace(array('#',' '),'',$code_project);
    $id_project = $sql->getval('id', DB_TABLE_PREFIX.'projects',"alias = '".$code_project."'");
    if(empty($id_project)) die('Проекта с таким кодом не найдено, возможно Вы ввели код с ошибкой.');
    $sql->upd(
        DB_TABLE_PREFIX.'auth_pers',
        "`code_project` = '{$code_project}', `role_project` = '0', `agree_normativ` = '{$agree_normativ}' ",
        "author_id = '".$_SESSION['SESS_AUTH']['ID']."'");

    $_SESSION['SESS_AUTH']['ALL']['code_project'] = $code_project;
    $_SESSION['SESS_AUTH']['ALL']['role_project'] = 0;
}
die('ok');
header("Location: /empty/cabinet/projects?ajax_data");
exit;
