<?php
$_DOC['conf']['tree_from_db'] = true;
// удаление элементов первого уровня
$_DOC['conf']['SEO']	= false;
$_DOC['conf']['seo_desc']	= "";
$_DOC['conf']['seo_keywords']	= "";

//кнопочка загрузки csv
$_DOC['conf']['csv']	= false; // для меню администратора и выполнения /empty/csv команды
// запреты на администрирование

// удаление элементов первого уровня
$_DOC['conf']['adm_nokill_first']	= false;

// добавление элементов первого уровня
$_DOC['conf']['adm_noadd_first']	= false;

// перемещение элементов
$_DOC['conf']['adm_nomove']			= false;

// автоматичекий показ поддокументов
$_DOC['conf']['no_subtree_all']		= false;

// на каких страницах показывать форму обратной связи
$_DOC['conf']['obrsv']				= array(/*"contacts/","contacts","contacts/en/"*/);

$_DOC['conf']['SEEALSO'] = array(
);
$_DOC['conf']['dbmodule'] = false; // возможность подключения каталога к doctxt

$_DOC['conf']['add_tree_fields'] = 'icon';

/**** Доступы к каталогам *****/
if(isset($_SESSION['SESS_AUTH']['ALL']['account_type'])){
    // подключим конфиг каталогов соответствующий типу аккаунта
    switch ($_SESSION['SESS_AUTH']['ALL']['account_type']) {
        case ACCTYPE_ROOT:
        case ACCTYPE_MENTOR:
        case ACCTYPE_MODER:
            unset($_DOC['ADM_MENU']);
            break;
        case ACCTYPE_EDIT:
        case ACCTYPE_ORG:
        case ACCTYPE_ADM:

            $_DOC['ADM_MENU'] = array("/doctxt/?list" => array("/ico/a_tree.gif", Main::get_lang_str('struct_site', 'doctxt'), $_DOC['MODULE_NAME']));
            $_DOC['permissions'][$_SESSION['SESS_AUTH']['LOGIN']] = true;
            break;
    }
}