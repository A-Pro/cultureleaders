<?
$form['path'] = str_replace('/doctxt/','/',$form['path']);
global $_KAT;
$addDb = array('');
foreach($_KAT['TABLES'] as $dbname=>$tabName){
    $hideDb = array('settings', 'auth','faqtoadmin','admin_faq','admin_faq_youtube','core_menu');
    if(!in_array($dbname,$hideDb))
        if($_KAT[$dbname]['hidden'] && !strstr($_KAT['FILES'][$dbname], 'coredb_')){
            $addDb[$dbname] = $_KAT['TITLES'][$dbname];
        }
}

$FORM_DATA= array (
    'title' => array (
        'field_name' => 'title',
        'name' => 'form[title]',
        'title' => 'Заголовок',
        'maxlen' => '255',
        'must' => '1',
        'style' => 'width:100%',
        'type' => 'textbox',
        'readonly' => 0,
    ),/*
    'anons' => array (
        'field_name' => 'anons',
        'name' => 'form[anons]',
        'title' => 'Анонс',
        'maxlen' => '1024',
        'must' => '0',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '4',
    ),*/
    'cont' => array (
        'field_name' => 'cont',
        'name' => 'form[cont]',
        'title' => 'Текст',
        'maxlen' => '20000',
        'must' => '0',
        'type' => 'textarea',
        'style' => 'width:100%',
        'rows' => '15',
        'wysiwyg'	=> 'tinymce',
    ),

   /* 'icon' => array (
        'field_name' => 'icon',
        'name' => 'form[icon]',
        'title' => 'icon',
        'maxlen' => '255',
        'must' => '0',
        'style' => 'width:100%',
        'type' => 'radiobtn',
        'arr'	=> array(
            ''=> ' без иконки',
            'dashboard'=>' <i class="h4 icon-dashboard"></i>',
            'group'=>' <i class="h4 icon-group"></i>',
            'beer'=>' <i class="h4 icon-beer"></i>',
            'legal'=>' <i class="h4 icon-legal"></i>',
            'microphone'=>' <i class="h4 icon-microphone"></i>',
            'dollar'=>' <i class="h4 icon-dollar"></i>',
            'cogs'=>' <i class="h4 icon-cogs"></i>',
            'wrench'=>' <i class="h4 icon-wrench"></i>',
        ),
    ),*/
    /*
    'dbmodule' => array (
        'field_name' => 'dbmodule',
        'name' => 'form[dbmodule]',
        'title' => 'Прикрепить каталог',
        'maxlen' => '255',
        'must' => '0',
        'style' => 'width:100%',
        'type' => 'select',
        'arr'	=> $addDb, 
    ),*/

    'doc' => array(
        'field_name' => 'doc', // должно совпадать с 'name'!!!
        'name'	=> 'doc',
        'title' => 'Изображение 1',
        'type'	=> 'photo',
        'admwidth' => '300',
        'sub_type'	=> 'photo',
        'newname_func'	=> 'DOCTXT::get_file_name("'.uniqid("photo").'", false)',
        'path'	=> DOCTXT::set_data_link( $form['path'].'file', $file ), 
        'abspath'	=> DOCTXT::set_data_path($form['path'].'file', $file),
    ),

    'doc2' => array(
        'field_name' => 'doc2', // должно совпадать с 'name'!!!
        'name'	=> 'doc2',
        'title' => 'Изображение 2',
        'type'	=> 'photo',
        'admwidth' => '300',
        'sub_type'	=> 'photo',
        'newname_func'	=> 'DOCTXT::get_file_name("'.uniqid("photo2").'", false)',
        'path'	=> DOCTXT::set_data_link( $form['path'].'file', $file ),
        'abspath'	=> DOCTXT::set_data_path($form['path'].'file', $file),
    ),

    'doc3' => array(
        'field_name' => 'doc3', // должно совпадать с 'name'!!!
        'name'	=> 'doc3',
        'title' => 'Изображение 3',
        'type'	=> 'photo',
        'admwidth' => '300',
        'sub_type'	=> 'photo',
        'newname_func'	=> 'DOCTXT::get_file_name("'.uniqid("photo3").'", false)',
        'path'	=> DOCTXT::set_data_link( $form['path'].'file', $file ),
        'abspath'	=> DOCTXT::set_data_path($form['path'].'file', $file),
    ),
    'hidden' => array (
        'field_name' => 'hidden',
        'name' => 'form[hidden]',
        'title' => 'Скрыто',
        'must' => 0,
        'maxlen' => 20,
        'type' => 'hidden',
    ),
);

