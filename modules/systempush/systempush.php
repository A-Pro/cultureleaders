<?php
include_once "systempush.inc.php";

global $_PROJECT, $_CHAT, $_CORE, $SQL_DBCONF, $_KAT; // in user/index.inc

if (!$_CORE->dll_load('class.DBCQ'))
	die ($_CORE->error_msg());

$oSystemPush = new SystemPush();

switch ($Cmd) {
	case "content/title":
	case "module/title":
		echo "Системные PUSH-сообщения";
		break;
	case "content/path":
	case "module/desc":
		break;

    case "show":
        if( empty($_SESSION['SESS_AUTH']['ID']) ) break;

        // подгрузим только те что еще не прочитанные и добавим их в выпадающий список
        $no_read = $oSystemPush->get_messages($_SESSION['SESS_AUTH']['ID'], 1 );
        include 'show.inc.html';
        
        break;

    
    case "send":

        $localsocket = 'tcp://127.0.0.1:1236';
        // получаем список всех сообщений со статусом 0 - новые
        $messages = $oSystemPush->get_messages('all');
        if(!empty($messages)) {
            // соединяемся с локальным tcp-сервером
            $instance = stream_socket_client($localsocket);
            $data = json_encode($messages);
            echo $data."<br>"."<br>";
            // отправляем сообщение
            fwrite($instance, $data  . "\n");
        }
        die("Ok");
        break;

    case "readed":

        if(!empty($_POST['ids']) && is_array($_POST['ids']) ){
            foreach($_POST['ids'] as $id){
                $oSystemPush->message_readed(intval($id));
            }
        }
        die();
        break;

    case "install":
        // создадим необходимые таблицы
        $oSystemPush->create_table();
        break;

    case "test_message":
        // добавим текущему пользователю одно сообщение от системы
        $data = array(
            'from_id' => 0, // 0 - системное
            'to_id' => 6, // author_id кому сообщение
            'title' => "Заголовок (тема)",
            'message' => "Текст тестового сообщения2",
        );
        $oPush->add_message($data);

        break;

	default: 

}