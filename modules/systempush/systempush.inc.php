<?php global $_SERVER;

class SystemPush
{

    public $SERVER;
    public $SQL_DBCONF;
    public $rooms;
    public $connects;

    public function __construct()
    {
        $this->sql = new SQL();
        $this->local_srv = "tcp://127.0.0.1:1236";
    }


    public function get_messages($user_id, $state = 0, $limit = 20)
    {

        $this->sql->query('SET NAMES utf8');

        $where = '1';
        if( $user_id != 'all' ){
            $where .= " AND to_id = ".$user_id;
        }
        if( !is_array($state)) {
            $where .= " AND state = '" . $state . "'";
            $tail = " ORDER BY ts DESC";
        }else{
            $where .= " AND state IN (" . implode(",", $state) . ")";
            $tail = " ORDER BY ts DESC";
        }
        $tail .= " LIMIT ".$limit;
        $messages = $this->sql->getall('*', DB_TABLE_PREFIX . 'push_messages', $where, $tail);
        return $messages;
    }

    public function message_sended($id)
    {
        $where = "id = ".$id;
        $this->sql->upd(DB_TABLE_PREFIX . 'push_messages', "state = '1'" , $where);
    }

    public function message_readed($id)
    {
        $where = "id = ".$id;
        $this->sql->upd(DB_TABLE_PREFIX . 'push_messages', "state = '2'" , $where);
    }

    public function create_table()
    {
        $query = "CREATE TABLE IF NOT EXISTS `". DB_TABLE_PREFIX ."push_messages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `from_id` bigint(20) NOT NULL,
  `to_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `state` enum('0','1','2') NOT NULL DEFAULT '0' COMMENT '0 - не отправлен; 1 - отправлено; 2 - прочитано;',
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=1";

        $this->sql->query($query);
    }

    public function add_message($data)
    {
        if(!isset($data['from_id']) || !isset($data['to_id']) || !isset($data['title']) || !isset($data['message'])){
            return false;
        }
        
        $this->sql->query('SET NAMES utf8');
        
        $res = $this->sql->ins(
            DB_TABLE_PREFIX ."push_messages",
            "`from_id`, `to_id`, `title`, `message`",
            "{$data['from_id']}, {$data['to_id']}, '{$data['title']}', '{$data['message']}'"
            );
        if(!$res->Result) return false; // ошибка запроса
        return true;
    }

    public function checkUser($id)
    {
        $result = $this->sql->getrow('*', DB_TABLE_PREFIX . 'auth_pers', "`author_id` = '{$id}'");
        return $result;
    }


}