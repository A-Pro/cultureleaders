#!/usr/bin/php

<?php
global $_SERVER, $SQL_DBCONF;
ini_set('display_errors','Off');
$core_path = '/home/vmcrm/lib/sited_core3.git';


include "/home/vmcrm/web/panel.vmcrm.ru/public_html/console_inc.php";

    if (empty($_SERVER['DOCUMENT_ROOT']))
        $_SERVER['DOCUMENT_ROOT'] = "/home/vmcrm/web/panel.vmcrm.ru/public_html";


    include_once 'systempush.inc.php';
        
//    require_once $_SERVER['DOCUMENT_ROOT'] . '/conf.phtml';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/debug.phtml';
    require_once $_SERVER['DOCUMENT_ROOT'] . '/lib/Workerman/Autoloader.php';
    require_once $core_path . "/lib/class.DBCQ.php";
    require_once $core_path . '/core.lib/class.Main.php';
    require_once $core_path . '/core.lib/sep/class.STR.php';

    use Workerman\WebServer;
    use Workerman\Worker;
    
    $oSystemPush = new SystemPush;

/*    if(!isset($oSystemPush->sql)){
        $oSystemPush->sql = new SQL;
        $oSystemPush->sql->query('SET NAMES utf8');
    }*/
    $oSystemPush->SERVER = $_SERVER;
    $oSystemPush->SQL_DBCONF = $SQL_DBCONF;
    
    // массив коннектов пользователей
    $oSystemPush->rooms = array();

    $cert_path = str_replace ( 'public_html', '', $_SERVER['DOCUMENT_ROOT'] );

    // SSL context.
    $context = array(
        'ssl' => array(
            'local_cert'  => $cert_path . 'server.pem',
            'local_pk'    => $cert_path . 'server.key',
            'verify_peer' => false,
        )
    );

    // создаём ws-сервер, к которому будут подключаться все наши пользователи
     $ws_worker = new Worker("websocket://0.0.0.0:2053", $context);
     
     // for SSL 
     $ws_worker->transport = 'ssl';
     
     //$ws_worker = new Worker("websocket://127.0.0.1:8001");

    
    // создаём обработчик, который будет выполняться при запуске ws-сервера
    $ws_worker->onWorkerStart = function() use (&$oSystemPush)
    {
        // создаём локальный tcp-сервер, чтобы отправлять на него сообщения из кода нашего сайта
        $inner_tcp_worker = new Worker("tcp://127.0.0.1:1236");
        // создаём обработчик сообщений, который будет срабатывать,
        // когда на локальный tcp-сокет приходит сообщение
        $inner_tcp_worker->onMessage = function($connection, $data) use (&$oSystemPush) {
            global $_SERVER, $SQL_DBCONF;
            $_SERVER = $oSystemPush->SERVER;
            $SQL_DBCONF = $oSystemPush->SQL_DBCONF;
            $oSystemPush->sql->connect();
            $messages = json_decode($data,true);
            foreach($messages as $mess) {
                // отправляем сообщение пользователю по userId

                echo "Data from " . $mess['from_id'] . ' to ' . $mess['to_id'] . ' : send message' ." \n";
                // print_r($mess);
                $data = json_encode($mess);

                if (isset($oSystemPush->rooms[$mess['to_id']])) {
                    if(is_array($oSystemPush->rooms[$mess['to_id']])){
                        // разошлем по всем конектам данного пользователя
                        foreach ($oSystemPush->rooms[$mess['to_id']] as $webconnection){
                            $webconnection->send($data);
                        }
                    }
                }
                $oSystemPush->message_sended($mess['id']);
            }
            $oSystemPush->sql->destroy();

        };
        $inner_tcp_worker->listen();
    };
    
    $ws_worker->onConnect = function($connection) use (&$oSystemPush)
    {
        $connection->onWebSocketConnect = function($connection) use (&$oSystemPush)
        {
            global $_SERVER, $SQL_DBCONF;
            $_SERVER = $oSystemPush->SERVER;
            $SQL_DBCONF = $oSystemPush->SQL_DBCONF;
            $oSystemPush->sql->connect();
            $user_id = $_GET['user_id']; // id пользователя (author_id)
            // проверим наличие пользователя с таким id
            if($result = $oSystemPush->checkUser($user_id) == false){
                echo "From User not found\n$user_id\n".print_r($result,1)."\n\n";
                $oSystemPush->sql->destroy();
                return;
            }

            echo "Connection: " . $user_id  . " ". date('Y-m-d H:i:s'). "\n\n";
            // при подключении нового пользователя сохраняем get-параметр, который же сами и передали со страницы сайта
            $hash_connection = md5(json_encode($connection));
            $oSystemPush->rooms[$user_id][] = $connection;
            $oSystemPush->connects[$hash_connection] = $user_id;
            $oSystemPush->sql->destroy();
            print_r($oSystemPush->connects);
        };
    };
    
    $ws_worker->onClose = function($connection) use(&$oSystemPush)
    {
        // удаляем параметр при отключении пользователя
        $hash_connection = md5(json_encode($connection));
        $user_id = $oSystemPush->connects[$hash_connection];
        
        if (!empty($oSystemPush->rooms[$user_id])) {
            $id_connect = array_search($connection, $oSystemPush->rooms[$user_id]);
            unset( $oSystemPush->rooms[$user_id][$id_connect],$oSystemPush->connects[$hash_connection]  );
            echo "User " . $user_id. " disconnection ". date('Y-m-d H:i:s'). "\n";
        }else
            echo "User " . $user_id. " was not in the room ". date('Y-m-d H:i:s'). "\n";
                
        
        //$oSystemPush->sql->destroy();
    };
    
    // Run worker
    Worker::runAll();
