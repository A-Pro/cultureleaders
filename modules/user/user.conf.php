<?
global $_CONF,$_CORE;
$_USER['MODULE_NAME']	= 'user';
$_USER['PATH_MAIN']		= 'Содержание';
$_USER['PATH_NOLAST']	= false;
$_USER['Modules4Map']	= array( 'doctxt', 'news');
$_USER['NO_DOCTXT_IN_MENU'] = 'true';
$_USER['START_TITLE']   = "";
if ($_CORE->CURR_TPL != 'admin') {
    if (!empty($_CONF['settings']['sitename'])) {
        $_USER['START_TITLE'] = $_CONF['settings']['sitename']; // for EMAIL OB_SVYAZ + BASKET
    } else {
        $_USER['START_TITLE'] = "";
    }
}
?>