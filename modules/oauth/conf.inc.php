<?php

$config = array(
/*
    'client_id'     => 2,
    'client_secret' => '82f8c5a7f673ecbed63593f574408a07',
    'redirect_uri'  => 'http://cilci.rsv.bizml.ru/oauth/back',
*/
    'client_id'     => 7,
    'client_secret' => '81q3qgadfb712cf3qefghjn072401a07',
    'redirect_uri'  => 'http://cultureleaders.ru/oauth/back',

    'urls' => array(
        'authorize'     => 'https://auth.rsv.ru/oauth2/auth',
        'token'         => 'https://auth.rsv.ru/oauth2/token',
        'refresh_token' => 'https://auth.rsv.ru/oauth2/refresh-token',
        'user_info'     => 'https://auth.rsv.ru/api/v1/user/info',
    ),

    'fields' => array(
        'author_comment' => 'user_name',
        'author_login' => 'user_email',
    ),
    'after_login' => array(
        ACCTYPE_ROOT => '/cabinet',
        ACCTYPE_MENTOR => '/cabinet',
        ACCTYPE_EDIT => '/db/projects/',
        ACCTYPE_ORG => '/db/projects/',
        ACCTYPE_MODER => '/db/projects/',
        ACCTYPE_ADM => '/admin'
    )
);

// конфиг для дев версии
if ($_SERVER['SERVER_NAME']== 'rsv-dev.staff-hub.ru') {
        $config['client_id']        = 3;
        $config['client_secret']    = 'e53a42016ce23971b4d40acf3bc09adb';
        $config['redirect_uri']     = 'http://rsv-dev.staff-hub.ru/oauth/back';
}
// '&client_password='.$config['client_secret'].
$config['urls']['code'] = 'https://auth.rsv.ru/oauth2/auth?client_id='.$config['client_id'].'&redirect_uri='.$config['redirect_uri'].'&client_scope=test&response_type=code&state=123';