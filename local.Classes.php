<?php

/**
 * Controll Access to db tables for from_auth and from_group
 *
 *
 */

class Access {

    var $conf; // keep 'db' => 'self|all', if nope, then no any

    public function Access( $conf ){
        if (!empty($conf['access_db']))
            $this->conf = $conf['access_db'];
        else
            $this->conf = false;
    }
    /**
     * return where postfix with rules for user and group,
     *
     * if hidden = false - then dont add hidden rules and show all
     *
     * */
    public function get_where ( $db, $hidden = true, $tab_alias = '' ) {

        // default as ROOT
        $filter = " AND (".$tab_alias."from_auth = '".$_SESSION['SESS_AUTH']['ID']."' OR ".$tab_alias."from_group = '".$_SESSION['SESS_AUTH']['ID']."' )";

        if (!empty($this->conf) && !empty($_SESSION['SESS_AUTH']['ALL']['from_auth'])){
            // если права в принципе есть ... проверим на свои или все
            if (!empty($this->conf[$db])) {
                switch ($this->conf[$db]) {
                    // если можно не только свои то добавим видимость группы
                    case 'all':
                        $access = " OR ".$tab_alias."from_group = '".$_SESSION['SESS_AUTH']['ALL']['from_group']."' ";
                        break;
                }
            // если роль установлена, но на конкретную базу разрешения нет, то запретить
            } else {
                $access = " AND 0 ";
            }

            $filter	= " AND (".$tab_alias."from_auth = '".$_SESSION['SESS_AUTH']['ID']."' " . $access . ") ";

            // add hidden filter

        }
        if ( $hidden )
            $filter .= ' AND ('.$tab_alias.'hidden != 1 OR '.$tab_alias.'hidden IS NULL) ' ;

        return $filter;
    }
}

function debug($var){
    echo "debug info<pre>".print_r($var,1)."</pre>";
    return;
}


function project_group($alias, $mot_letter = false){
    global $_CONF,$_KAT,$_CORE;
    $result = '';
    $sql = new SQL();
    $items = $sql->getall(
        'author_comment, role_project, complit,alias, motivation_letter',
        DB_TABLE_PREFIX.'auth_pers',
        "code_project = '".$alias."' AND role_project >= 20",
        'ORDER BY role_project DESC'
    );
    if(is_array($items)) {
        foreach ($items as $item) {
            if (isset($_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']]) && in_array('accounts', $_KAT['permissions'][$_SESSION['SESS_AUTH']['LOGIN']])) {
                $result .= '<a style="color:#398bf7;" href="/db/accounts/?form[code_project]=' . $alias . '" target="_blank" >' . $item['author_comment'] . '</a> ~ ' .
                    $_CONF['role_project'][$item['role_project']] .
                    ' ~ '.'<b >Профиль заполнен на '.$item['complit'].'%</b>';
            } else {
                $result .= $item['author_comment'] . ' ~ ' . $_CONF['role_project'][$item['role_project']].
                    ' ~ '.'<b >Профиль заполнен на '.$item['complit'].'%</b>';
            }

            if($_CONF['OurChat']){
                if($_SESSION['SESS_AUTH']['ALL']['alias'] != $item['alias'] && $_CORE->CURR_TPL == 'def'){
                    $result .= ' ~ <a href="javascript:"  class="OurChat_show" data-alias="'.$item['alias'].'">Написать</a>';
                }
            }
            if($mot_letter) {
                $result .= '<br>Мотивационное письмо: '.$item['motivation_letter'] . '<br>';
            }
            $result .= '<br>';
        }
    }
    return $result;
}


function project_docs($alias,$stage){
    global $_CONF,$_KAT;
    $result = '';
    $sql = new SQL();
    $where = "project_id = '".$alias."'";
    switch ($stage){
        case 1:
            if($_CONF['settings']['stage1_off'] > '0000-00-00')
                $where .= " AND ts <= '".$_CONF['settings']['stage1_off']."'";
            elseif($_CONF['settings']['stage2_off'] > '0000-00-00')
                $where .= " AND ts < '".$_CONF['settings']['stage2_off']."'";
            break;
        case 2:
            if($_CONF['settings']['stage1_off'] > '0000-00-00')
                $where .= " AND ts > '".$_CONF['settings']['stage1_off']."'";
            if($_CONF['settings']['stage2_off'] > '0000-00-00')
                $where .= " AND ts <= '".$_CONF['settings']['stage2_off']."'";

            break;
        case 3:
            if($_CONF['settings']['stage2_off'] > '0000-00-00')
                $where .= " AND ts > '".$_CONF['settings']['stage2_off']."'";
            break;
        default:
            return '';
            break;
    }

    $items = $sql->getall(
        'name, doc,ts',
        DB_TABLE_PREFIX.'docs',
        $where,
        'ORDER BY ts ASC'
    );
    $num = 0;
    $path = '/data/db/f_docs/';
    $abs_path = $_SERVER['DOCUMENT_ROOT'].$path;
    if(is_array($items)){
        foreach ($items as $item){
            $num++;
            $f_size = FILE::f_size(filesize($abs_path . $item["doc"]),0);
            $f_ext = substr($item["doc"],strripos($item["doc"],'.'));
            $result .= $num.'. <a style="color:#398bf7;" href="'.$path . $item['doc'].'" target="_blank" >'.
                             $item['name'] . '</a>  ('.$f_ext.' '.$f_size.')<br>';
        }
    }
    return $result;
}

function show_url($url, $title, $prefix_url=''){
    $result = '';
    if(!empty($url)){
        if(empty($title)) $title = 'ссылка';
        $result = '<a style="color:#398bf7;" href="'.$prefix_url.$url.'" target="_blank">'.$title.'</a>';
    }
    return $result;
}