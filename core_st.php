<?php
$START_CORE	= getmicrotime();
// if (in_array($_SERVER['REQUEST_URI'], array('/admin/', '/admin')) && $_SERVER['HTTP_HOST'] != 'srv-kmd24.ru') exit;

/**
 *	core_st.php
 **/
ob_start();
session_start();

//error_reporting( E_ALL ^ E_NOTICE );
//error_reporting( 15);
$locale= setlocale(LC_ALL,'ru_RU.CP1251', 'ru_RU', 'ru');

#error_reporting( E_ALL & ~E_NOTICE);

// ini_set('display_errors','on');
header("X-Engine: lCore 0.3.0 Basic, (c) 2005-2019 Andrew Lyadkov / staff-hub");
header("Accept-Charset: utf-8");
header("Content-type: text/html; charset=utf-8");

define('ITEM_COPY', 'cop');
define('ITEM_CUT', 'cut');

GLOBAL $_CORE;

include "conf.phtml";
include "debug.phtml";
include $_CONF['CorePath'].'/core.lib/class.Main.php';
include_once "local.Classes.php";

//include_once "modules/systempush/systempush.inc.php";
//$_PUSH  = new SystemPush();

$_CORE	= new Main();
$_CORE->CONF['months'] = array (
    "01" => "января",
    "02" => "февраля",
    "03" => "марта",
    "04" => "апреля",
    "05" => "мая",
    "06" => "июня",
    "07" => "июля",
    "08" => "августа",
    "09" => "сентября",
    "10" => "октября",
    "11" => "ноября",
    "12" => "декабря"
);

$_CORE->CONF['month'] = array (
    "01" => "Январь",
    "02" => "Февраль",
    "03" => "Март",
    "04" => "Апрель",
    "05" => "Май",
    "06" => "Июнь",
    "07" => "Июль",
    "08" => "Август",
    "09" => "Сентябрь",
    "10" => "Октябрь",
    "11" => "Ноябрь",
    "12" => "Декабрь"
);
$_ACCESS = new Access( $_CONF );


// 10.07.2012 new feature with settings
if ($_CONF['FEATURES_USED']['settings']) {
    cmd('/db/settings',false,true);
}

include "email.phtml";
//$mazilla = urldecode($_SERVER['REQUEST_URI']);
//$ie 	 = STR::utf2win(urldecode($_SERVER['REQUEST_URI']));
//$some = get_browser(null, true);
//$ie = (preg_match("|^mozilla/.\.0 (compatible; msie 6\.0.*;.*windows nt 5\.1.*).*$|i",$_SERVER['HTTP_USER_AGENT']));


// 21.11.2013 for new apache
//
if (false !== ($qpos = strpos( $_SERVER['REQUEST_URI'], '?'))) {
    $tmp = $_SERVER['REQUEST_URI'];
    $_SERVER['REQUEST_URI'] = substr($tmp, 0, $qpos);
    $_SERVER['QUERY_STRING'] = substr($tmp, $qpos+1);
}

$_SERVER['REQUEST_URI']	= str_replace('//','/',$_SERVER['REQUEST_URI']);

$ie	= (strstr($_SERVER['HTTP_USER_AGENT'], "MSIE"));
$url = ($ie)
    ?STR::utf2win(urldecode($_SERVER['REQUEST_URI'])) // IE
    :urldecode($_SERVER['REQUEST_URI']); //firefox


if ($url!=$_SERVER['REQUEST_URI']) {
    $_SERVER['REQUEST_URI'] =  STR::wrong_keybord($url);
    if (!$ie) {
        header("Location: ".$_SERVER['REQUEST_URI']);
        exit;
    }
}

#
# Debug mode
#
$_CORE->set_debug( DEBUG );

/* UTM-метки */
if( empty($_SESSION['UTM']) && (isset($_GET['utm_source']) || isset($_GET['utm_medium']) || isset($_GET['utm_campaign']) || isset($_GET['utm_term']) || isset($_GET['utm_content']) ) ){
    $sql = new SQL();
    $utm = array();
    $_SESSION['UTM'] = $utm['alias'] = uniqid();
    $utm['utm_source'] = (!empty($_GET['utm_source'])) ? addslashes(htmlspecialchars($_GET['utm_source'])):'';
    $utm['utm_medium'] = (!empty($_GET['utm_medium'])) ? addslashes(htmlspecialchars($_GET['utm_medium'])):'';
    $utm['utm_campaign'] = (!empty($_GET['utm_campaign'])) ? addslashes(htmlspecialchars($_GET['utm_campaign'])):'';
    $utm['utm_term'] = (!empty($_GET['utm_term'])) ? addslashes(htmlspecialchars($_GET['utm_term'])):'';
    $utm['utm_content'] = (!empty($_GET['utm_content'])) ? addslashes(htmlspecialchars($_GET['utm_content'])):'';
    $sql->ins_set(DB_TABLE_PREFIX.'utm', $sql->get_set($utm),DEBUG);
}

#
# Module Result
#
$_CORE->PARSE_URL	= parse_url($_SERVER['REQUEST_URI']);
$_CORE->URL_PATH	= $_CORE->PARSE_URL['path'];
$_CORE->ALL_LANGS	= array( 'ru', 'en' );
if ($_CORE->URL_PATH == '/') $_CORE->URL_PATH	= '';


// В любом случае подключаем модуль и проверим админ ли.
$_CORE->IS_ADMIN = (isset($_SESSION['CORE_ADMIN']) && in_array($_SESSION['CORE_ADMIN'], array('admin','demo')));
$_CORE->IS_DEMO = (isset($_SESSION['CORE_ADMIN']) && $_SESSION['CORE_ADMIN'] == 'demo');
$_CORE->IS_ROOT = (isset($_SESSION['CORE_ROOT']) && $_SESSION['CORE_ADMIN'] == $_SESSION['CORE_ROOT']);

// Проверяем Язык вывода (СМОТРИМ ХВОСТЫ)
if (in_array( substr($_CORE->URL_PATH, -4), array( '/ru/','/en/') )){
    $_CORE->LANG 	= substr($_CORE->URL_PATH, -3,2);
    $_CORE->NOLANG_URL	= substr($_CORE->URL_PATH,0,-3);

}else{
    $_CORE->LANG =	'ru';
    $_CORE->NOLANG_URL	= $_CORE->URL_PATH;
}


// СМОТРИМ И ПАРСИМ ПРЕФИКСЫ
$MOD_CMD	= $_CORE->cmd_parse( $_SERVER['REQUEST_URI'] );
if ($MOD_CMD[0] == 'empty' || $MOD_CMD[0] == 'print' || $MOD_CMD[0] == 'clear' || $MOD_CMD[0] == 'def') {
    $_CORE->EMPTY_VERSION	= ($MOD_CMD[0] == 'empty');
    $_CORE->PRINT_VERSION	= ($MOD_CMD[0] == 'print');
    $_CORE->CLEAR_VERSION	= ($MOD_CMD[0] == 'clear');
    $_CORE->DEFAULT_VERSION	= ($MOD_CMD[0] == 'def');
    $_CORE->NOPRINT_URL		= $MOD_CMD[1];
    $_SERVER['REQUEST_URI']	= $MOD_CMD[1];
}
if($MOD_CMD[0] == 'data' ){
    $_SERVER['REQUEST_URI'] = str_replace('/data/db/','/access_data/',$_SERVER['REQUEST_URI']);
}
// ПАРСИМ АЛИАСЫ
#
# FULL NAME ALIASES
#
if ($_CONF['FEATURES_USED']['aliases'] && !in_array($MOD_CMD[0], array('templates', 'imglib', 'reimg')) ) {
    $RealName = cmd('/urlalias/name'.$_SERVER['REQUEST_URI']);
    if (!empty($RealName)){
        $_SERVER['REQUEST_URI'] = $RealName;
    }
}

if (!$_CONF['FEATURES_USED']['aliases']) {
    cmd('/db/nothing');
}
/* MAIN COMMAND DO */
$MOD_CMD	= $_CORE->cmd_parse( $_SERVER['REQUEST_URI'] );

/* Определяем шаблон  ( на случай если УЖЕ админ ) */
if ($_CORE->IS_ADMIN  && @$_SESSION['ADMIN']['TPL'] != 'public') {
    $_CORE->CURR_TPL	= 'admin';
}else {
    $_CORE->CURR_TPL	= 'def';
}
/*  ---------------- */



/* Tree Loading */
echo $_CORE->cmd_pexec( 'user/' );

//echo $_CORE->cmd_pexec( 'pagesnum/refresh' );

// Сделано для превью шаблона АДМИНИСТРАТОРОМ

if ($_CORE->PRINT_VERSION || $_CORE->DEFAULT_VERSION){
    $_CORE->IS_ADMIN = 0;
    $_CORE->CURR_TPL = 'def';
}

// для connects
$_CORE->BODY_REQUEST_URI = $_SERVER['REQUEST_URI'];

if (!in_array($_SERVER['REQUEST_URI'], array('','/','/en/') )){
    $LoadModule = ($CORE_BODY	= cmd($_SERVER['REQUEST_URI'], 'first'));

    $Template	=  (!empty($_CORE->MainTpl))  // для специальных шаблонов, а не только главная и внутри...
        ? 'altinner'
        : 'inner';

//	$Template	= 'index';
}else {
    $LoadModule = ($CORE_BODY	= $_CORE->cmd_pexec( 'index/' ));
    $Template	= 'index';
}

/* Определяем шаблон  ( на случай если только-только ) */
if ($_CORE->IS_ADMIN && @$_SESSION['ADMIN']['TPL'] != 'public') {
    $_CORE->CURR_TPL	= 'admin';
    if (!$_CORE->PRINT_VERSION && !$_CORE->CLEAR_VERSION && !$_CORE->DEFAULT_VERSION)
        $Template	= 'admin';
}else {
    $_CORE->CURR_TPL	= 'def';
}


/*  ---------------- */

if ($_CORE->LANG != 'ru' && empty($CORE_BODY)) {
    $CORE_BODY	= 'No English version for this Document';
}

// $_CORE->int_done_run(INT_TREE_LOADED);

if (!empty($CORE_BODY)){
	header('Content-type: text/html; charset=utf-8');

    if (!empty($_CORE->PRINT_VERSION))
        $Template = 'print'; //include_once($_CORE->PATHTPLS.$_CORE->CURR_TPL.'/design/print.tpl.html');
    elseif (!empty($_CORE->CLEAR_VERSION))
        $Template = 'clear'; //include_once($_CORE->PATHTPLS.$_CORE->CURR_TPL.'/design/clear.tpl.html');
    elseif (!empty($_CORE->DEFAULT_VERSION))
        $Template = 'inner'; //include_once($_CORE->PATHTPLS.$_CORE->CURR_TPL.'/design/inner.tpl.html');
    elseif (!empty($_CORE->EMPTY_VERSION))
        $Template = 'empty'; //include_once($_CORE->PATHTPLS.$_CORE->CURR_TPL.'/design/empty.tpl.html');
    if ($_CORE->LANG == 'en')  $Template .= 'en.';

    Main::comm_inc($Template . '.tpl.html', $f, 'design');
    include_once "$f";

}elseif($_SERVER['REQUEST_URI'] != '/' && !is_file($_SERVER['DOCUMENT_ROOT'].'/'.substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], '?', 0)))) {
    #
    # this means that we come from 404 so heed footer
    #
//	echo "FILE ".$_SERVER['REQUEST_URI']." not found";
    $_CORE->return404();
    ob_start();
    include "404.html";
    $CORE_BODY = ob_get_contents();
    ob_end_clean();
    $_PROJECT['TPL:NOLEFT'] = true;
    include_once($_CORE->PATHTPLS.$_CORE->CURR_TPL.'/design/inner.tpl.html');

} else {
    echo "Empty CORE_BODY and Not home page needed.";
}

//if (in_array($_SERVER['REQUEST_URI'], array ('','/'))){
//	if (file_exists('index.php'))
//		include 'index.php';
//	elseif (file_exists('index.html'))
//		include 'index.html';
//}
$_CORE->SIZE	= ob_get_length();

_hide_foot();
//ob_start();
//print_r($GLOBALS);
//$tmp = ob_get_length();
//ob_end_flush();

#
# ALIASES
#
if (!$_CORE->IS_ADMIN){
//	$_CORE->int_done_run("CORE_OUTPUT_BEF");
//	echo $_CORE->error_msg();
    $cont = (is_object('URLALIAS')) ? URLALIAS::conv_buffer()	: ob_get_contents();
    ob_end_clean();
    echo $cont;
}else{
//	$_CORE->int_done_run("CORE_OUTPUT_BEF");
//	echo $_CORE->error_msg();
    ob_end_flush();
}
if (DEBUG) {
    echo "<!--";
    // print_r($_CORE->CMD_HISTORY);
    // print_r($_SESSION['SESS_AUTH']);
    echo "-->";
}

//$_CORE->int_done_run("CORE_OUTPUT_AFT");

//header("Content-Length: ".$_CORE->SIZE);
//ob_end_flush();

function _hide_foot(){
    global $START_CORE, $_CORE;
    $STOP_CORE	= getmicrotime();
    ?>
    <!-- <?=$_CORE->ver?> <?=$_SERVER['REQUEST_URI']?> :: time: <?=$STOP_CORE - $START_CORE?> | Size <?=$_CORE->SIZE?> | <?=$_CORE->log_id;?> -->
    <?
}

/**
 *
 */
function _core_worked(){
    global $START_CORE;
    $NOW	= getmicrotime();
    return sprintf( "%0.4f",$NOW - $START_CORE);
}

function getmicrotime(){
    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}

function _core_debug( $str ){
    global $_CORE;
    echo "<HR><B>DEBUGGING VALUE:<B> >$str<<br><pre>".print_r($_CORE->log_tail_win(3))."<HR>";
}

?>
